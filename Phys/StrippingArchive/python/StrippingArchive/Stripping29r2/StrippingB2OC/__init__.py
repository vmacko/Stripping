
_selections = ('StrippingB2DDphi', 'StrippingB2DXD2HMuNu', 'StrippingB2nbody', 'StrippingB2nbody', 'StrippingB2nbody', 'StrippingBeauty2Charm')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
