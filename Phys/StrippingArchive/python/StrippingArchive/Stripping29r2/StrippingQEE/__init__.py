
_selections = ('StrippingA1MuMu', 'StrippingA2MuMu', 'StrippingA2MuMuSameSign', 'StrippingConvertedPhoton', 'StrippingDY2MuMu', 'StrippingDY2ee', 'StrippingDisplVertices', 'StrippingDitau', 'StrippingExotica', 'StrippingFullDiJets', 'StrippingH24Mu', 'StrippingHltQEE', 'StrippingHltQEE', 'StrippingInclQQ', 'StrippingLFVExotica', 'StrippingLLP2MuX', 'StrippingLb2dp', 'StrippingLowMultINC', 'StrippingMicroDiJets', 'StrippingMuMuSS', 'StrippingSbarSCorrelations', 'StrippingSingleTrackTIS', 'StrippingStrangeBaryons', 'StrippingStrangeBaryonsNoPID', 'StrippingTaggedJets', 'StrippingWJets', 'StrippingWJets', 'StrippingWMu', 'StrippingWRareDecay', 'StrippingWe', 'StrippingZ02MuMu', 'StrippingZ02ee', 'StrippingZ0RareDecay')

for _sel in _selections :
    try :
        __import__( '%s.%s'  % ( __name__, _sel ) )
    except Exception, x:
        print '[WARNING] Submodule %s.%s raises the exception "%s" and will be skipped !' % ( __name__,_sel,x )

from sys import modules as _modules
_this = _modules[__name__]

_strippingKeys = filter ( lambda x : x[:9]=='Stripping',
                          locals().keys())

_strippingModules = [getattr(_this, _k) for _k in _strippingKeys]
