__author__ = ['G. Dujany, G. Mancinelli, J. Serrano']
__date__ = '24/05/2010,15/01/2014, 19/10/2017'
__version__ = '$Revision: 1.3 $'
# Stripping line by V. Gligorov for B->mu tau, B->tau tau, B->D mu and B->D D
# Strongly modified for Stripping20 by Ch. Elsasser
# added Dpi mode by G. Mancinelli
# Strongly modified for S20rxp1 by G. Mancinelli and J. Serrano
# Added new lines for DD0 by G. Dujany for S24r1p1
# Added new selection for 3pi3pi and 3pimu final states by G. Mancinelli and J. Serrano for S28r1p1
from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import mm

"""
  B->TauTau, B->TauMu
  """
__all__ = (
  'B2XTauConf'
  ,  'default_config'
  )

default_config =  {
  'NAME'              : 'B2XTau',
  'BUILDERTYPE'       :       'B2XTauConf',
  'WGs'    : [ 'RD' ],
  'CONFIG'    : {

  'PT_HAD_ALL_FINAL_STATE'        : '250',  # MeV
  'P_HAD_ALL_FINAL_STATE'         : '2000', # MeV
  'IPCHI2_HAD_ALL_FINAL_STATE'    : '16',    # dimensionless
  'TRACKCHI2_HAD_ALL_FINAL_STATE' : '4',    # dimensionless
  'TRGHOPROB_HAD_ALL_FINAL_STATE' : '0.4',    # dimensionless
  #
  'PT_MU'                         : '1000', # MeV
  'P_MU'                          : '6000', # MeV
  'IPCHI2_MU'                     : '16',   # MeV
  'TRACKCHI2_MU'                  : '4',    # dimensionless
  'TRGHOPROB_MU'                  : '0.4', # dimensionless

  #
  'PT_B_TT'                       : '1900', # MeV
  'PT_B_TT_HIGH'                  : '2000',# MeV
  'PT_B_TM'                       : '1900', # MeV
  'PT_B_TM_HIGH'                  : '5000', # MeV
  'VCHI2_B'                       :   '90', # dimensionless
  'FDCHI2_B'                      : '225',  # dimensionless
  'FD_B'                          : '90',   # mm
  'FD_B_MU'                       : '35',   # mm
  'DIRA_B'                        : '0.99', # dimensionless
  'MASS_LOW_B'                    : '2000', # MeV
  'MASS_HIGH_B'                   : '7000', # MeV
  'MCOR_LOW_B'                    :    '0', # MeV
  'MCOR_HIGH_B'                   :'10000', # MeV


  #
  'PT_B_CHILD_BEST'               : '2000', # MeV
  'IPCHI2_B_CHILD_BEST'           : '16',   # dimensionless
  'PT_B_TAU_CHILD_BEST'           : '4000', # MeV
  'IPCHI2_B_TAU_CHILD_BEST'       : '150',   # dimensionless
  'IPCHI2_B_TAU_CHILD_WORSE'       : '16',   # dimensionless
  'PT_B_PIONS_TOTAL'              :  '7000',# MeV
  'PT_B_MU_PIONS_TOTAL'           :  '2500',# MeV
  'IPCHI2_B_TAU_MU'               :  '50',
  'IPCHI2_B_MU'                   :  '200',
  'B_TAUPI_2NDMINIPS'             :  '20',  # dimensionless
  'FDCHI2_TAU'                    :  '4000',
  'VCHI2_B_TAU_MU'                : '12',
  #
  'MASS_LOW_D'                    : 1750 * MeV,
  'MASS_HIGH_D'                   : 2080 * MeV,
  'MASS_LOW_Ds'                   : 1938 * MeV,
  'MASS_HIGH_Ds'                  : 1998 * MeV,
  'MASS_LOW_Dmother'                 : 1800 * MeV,
  'MASS_HIGH_Dmother'                : 2030 * MeV,
  'APT_D'                         : 800 * MeV,
  'AMAXDOCA_D'                    : 0.2 * mm,
  'MaxPT_D'                       : 800 * MeV,
  'PT_D'                          : 1000 * MeV,
  'DIRA_D'                        : '0.99',
  'VCHI2_D'                       : '16',
  'FDCHI2_D'                      : '16',
  'VDRHOmin_D'                    : 0.1 * mm,
  'VDRHOmax_D'                    : 7.0 * mm,
  'VDZ_D'                         : 5.0 * mm,
  #
  'MASS_LOW_TAU'                    : 400 * MeV,
  'MASS_HIGH_TAU'                   : 2100 * MeV,
  'MASS_LOW_TAUmother'                    : 500 * MeV,
  'MASS_HIGH_TAUmother'                   : 2000 * MeV,
  #
  #

  'CUTBASED_M_TAU_HIGH'             : '1592', # MeV
  'CUTBASED_M_TAU_LOW'              : '829', # MeV
  'CUTBASED_PROBNNPI'               : '0.122',
  'CUTBASED_MISS_MASS'              : '876', # MeV
  'CUTBASED_MCORR_B_HIGH'           : '8600', # MeV
  'CUTBASED_MCORR_B_LOW'            : '3600', # MeV
  'CUTBASED_M_B'                    : '2865', # MeV
  #                   'CUTBASED_TAU_TAU_LOW'            : '-10.5', # ps
  #                   'CUTBASED_TAU_TAU_HIGH'           : '-0.5', # ps
  'CUTBASED_TAU_TAU_LOW'            : '-0.56', # ps
  'CUTBASED_TAU_TAU_HIGH'           : '1.960', # ps
  'CUTBASED_TAU_BPVVDRHO'           : '0.33', # mm
  'CUTBASED_VCHI2_TAU'              :   '20.6', # dimensionless
#
  'B2TauTau_3pi3pi_TOSLinePrescale'  : 1,
  'B2TauTau_3pi3pi_TOSLinePostscale' : 1,
  'B2TauTau_3pi3pi_SameSign_TOSLinePrescale'  : 1,
  'B2TauTau_3pi3pi_SameSign_TOSLinePostscale' : 1,
  'B2TauMu_3pi_TOSLinePrescale'  : 1,
  'B2TauMu_3pi_TOSLinePostscale' : 1,
  'B2TauMu_3pi_SameSign_TOSLinePrescale'  : 1,
  'B2TauMu_3pi_SameSign_TOSLinePostscale' : 1,
  'B2TauTau_TOSLinePrescale'      : 1,
  'B2TauTau_TOSLinePostscale'     : 1,
  'B2DD_LinePrescale'             : 1,
  'B2DD_LinePostscale'            : 1,
  'B2DD0_LinePrescale'             : 1,
  'B2DD0_LinePostscale'            : 1,
  'B2TauMu_TOSLinePrescale'       : 1,
  'B2TauMu_TOSLinePostscale'      : 1,
  'B2DPi_LinePrescale'            : 1,
  'B2DPi_LinePostscale'           : 1,
  'B2TauTau_SameSign_TOSLinePrescale'      : 1,
  'B2TauTau_SameSign_TOSLinePostscale'     : 1,
  'B2DD_SameSign_LinePrescale'             : 0.5,
  'B2DD_SameSign_LinePostscale'            : 1,
  'B2TauMu_SameSign_TOSLinePrescale'       : 0.5,
  'B2TauMu_SameSign_TOSLinePostscale'      : 1,
  'B2DPi_SameSign_LinePrescale'            : 0.5,
  'B2DPi_SameSign_LinePostscale'           : 1,
  'HLT_DECISIONS_HAD'    : {'Hlt2Topo(2|3|4)Body.*Decision%TOS' : 0},
#  'HLT_DECISIONS_HAD'    : {'Hlt2(Topo2Body|Topo3Body|Topo4Body).*Decision%TOS' : 0},
  'HLT_DECISIONS_MUON'  : {"Hlt2(TopoMu|SingleMuon).*Decision%TOS": 0},
  'RelatedInfoTools'      : [
  #1
  { "Type" : "RelInfoBstautauMuonIsolationBDT"
    , "Variables" : ['BSTAUTAUMUONISOBDTFIRSTVALUE', 'BSTAUTAUMUONISOBDTSECONDVALUE','BSTAUTAUMUONISOBDTTHIRDVALUE']
   , "Location"  : "MuonIsolationBDT"
    },
  #2
  { "Type" : "RelInfoBstautauMuonIsolation"
    , "Variables" : ['BSTAUTAUMUONISOFIRSTVALUE', 'BSTAUTAUMUONISOSECONDVALUE']
     , "Location"  : "MuonIsolation"
    },
  #3
  { "Type" : "RelInfoBstautauTauIsolationBDT"
    , "Variables" : ['BSTAUTAUTAUISOBDTFIRSTVALUETAUP', 'BSTAUTAUTAUISOBDTSECONDVALUETAUP','BSTAUTAUTAUISOBDTTHIRDVALUETAUP','BSTAUTAUTAUISOBDTFIRSTVALUETAUM', 'BSTAUTAUTAUISOBDTSECONDVALUETAUM','BSTAUTAUTAUISOBDTTHIRDVALUETAUM']
      , "Location"  : "TauIsolationBDT"
    },
  #4
  { "Type" : "RelInfoBstautauTauIsolation"
    , "Variables" : ['BSTAUTAUTAUISOFIRSTVALUETAUP', 'BSTAUTAUTAUISOSECONDVALUETAUP','BSTAUTAUTAUISOFIRSTVALUETAUM', 'BSTAUTAUTAUISOSECONDVALUETAUM']
     , "Location"  : "TauIsolation"
    },
  #5

 { "Type" : "RelInfoBstautauTrackIsolationBDT"
    , "Variables" : ['BSTAUTAUTRKISOBDTFIRSTVALUETAUPPIM', 'BSTAUTAUTRKISOBDTSECONDVALUETAUPPIM','BSTAUTAUTRKISOBDTTHIRDVALUETAUPPIM','BSTAUTAUTRKISOBDTFIRSTVALUETAUPPIP1', 'BSTAUTAUTRKISOBDTSECONDVALUETAUPPIP1','BSTAUTAUTRKISOBDTTHIRDVALUETAUPPIP1','BSTAUTAUTRKISOBDTFIRSTVALUETAUPPIP2', 'BSTAUTAUTRKISOBDTSECONDVALUETAUPPIP2','BSTAUTAUTRKISOBDTTHIRDVALUETAUPPIP2','BSTAUTAUTRKISOBDTFIRSTVALUETAUMPIP','BSTAUTAUTRKISOBDTSECONDVALUETAUMPIP','BSTAUTAUTRKISOBDTTHIRDVALUETAUMPIP','BSTAUTAUTRKISOBDTFIRSTVALUETAUMPIM1','BSTAUTAUTRKISOBDTSECONDVALUETAUMPIM1','BSTAUTAUTRKISOBDTTHIRDVALUETAUMPIM1','BSTAUTAUTRKISOBDTFIRSTVALUETAUMPIM2', 'BSTAUTAUTRKISOBDTSECONDVALUETAUMPIM2','BSTAUTAUTRKISOBDTTHIRDVALUETAUMPIM2']
     , "Location"  : "TrackIsolationBDT"
    },

  #6
  { "Type" : "RelInfoBstautauTrackIsolation"
    , "Variables" : ['BSTAUTAUTRKISOFIRSTVALUETAUPPIM', 'BSTAUTAUTRKISOFIRSTVALUETAUPPIP1','BSTAUTAUTRKISOFIRSTVALUETAUPPIP2', 'BSTAUTAUTRKISOFIRSTVALUETAUMPIP','BSTAUTAUTRKISOFIRSTVALUETAUMPIM1', 'BSTAUTAUTRKISOFIRSTVALUETAUMPIM2']
     , "Location"  : "TrackIsolation"
    },
  #7
  { "Type" : "RelInfoBstautauCDFIso"
    , "Variables" : ['BSTAUTAUCDFISO']
     , "Location"  : "CDFIso"
    },
  #8
  { "Type" : "RelInfoBstautauZVisoBDT"
    , "Variables" : ['ZVISOTAUP','ZVISOTAUM']
      , "Location"  : "ZVisoBDT"
    },
  ]
  },
  'STREAMS'     : ['Bhadron']
  }

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import FilterDesktop, CombineParticles, OfflineVertexFitter, DaVinci__N3BodyDecays
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from StandardParticles import StdNoPIDsKaons, StdNoPIDsPions, StdTightMuons
from StandardParticles import StdLoosePions, StdTightPions
from StandardParticles import StdAllLoosePions,StdAllNoPIDsPions
from StandardParticles import StdLooseKaons


from copy import deepcopy

default_name = "B2XTau"
HLT_DECISIONS_HAD   = "Hlt2Topo(2|3|4)Body.*Decision"
HLT_DECISIONS_MUON  = "Hlt2(TopoMu|SingleMuon).*Decision"

class B2XTauConf(LineBuilder) :

  """
    Builder for B->TauTau, B->TauMu
    """

  TauTau_TOSLine    = None
  TauTau_SS_TOSLine = None
  TauMu_TOSLine     = None
  TauMu_SS_TOSLine  = None
  DPi_Line       = None
  DPi_SS_Line    = None
  DD_Line        = None
  DD_SS_Line     = None
  TauTau_3pi3pi_TOSLine = None
  TauTau_3pi3pi_SS_TOSLine = None
  TauMu_3pi_TOSLine = None
  TauMu_3pi_SS_TOSLine = None

  __configuration_keys__ = default_config['CONFIG'].keys()


  def __init__(self, name, config):
    LineBuilder.__init__(self, name, config)
    #

    #
    # Selections
    #
    self.selPions        = self._makePions(      name    = "PionsFor"+name,
                                      config  = config)
    self.selMuons        = self._makeMuons(      name    = "MuonsFor"+name,
                                      config  = config)
    self.selPionsForD        = self._makePionsForD(      name    = "PionsDFor"+name,
                                      config  = config)
    self.selKaonsForD        = self._makeKaonsForD(      name    = "KaonsDFor"+name,
                                      config  = config)
    self.rawTau          = DataOnDemand(Location = "Phys/StdTightDetachedTau3pi/Particles")

    self.selTau          = self._filterTau(name       = name+"_TauFilter",
                                      tauInput   = self.rawTau,
                                      config     = config)

    self.selD            = self._makeD(name       = "DFor"+name,
                                  pionSel = self.selPionsForD,
                                  kaonSel = self.selKaonsForD,
                                  config     = config)

    self.selD0            = self._makeD0(name       = "D0For"+name,
                                         pionSel = self.selPionsForD,
                                         kaonSel = self.selKaonsForD,
                                         config     = config)
    #
    self.selB2TauTau,self.selB2DD     = self._makeB2XX(   name    = name,
                                                                 tauSel  = self.selTau,
                                                                 DSel    = self.selD,
                                                                 config  = config)
    self.selB2TauTauSS,self.selB2DDSS = self._makeB2XXSS( name    = name,
                                               tauSel  = self.selTau,
                                               DSel    = self.selD,
                                               config  = config)

    self.selB2TauMu     = self._makeB2XMu(  name    = name,
                                              tauSel  = self.selTau,
                                              muonSel = self.selMuons,
                                              config  = config)

    self.selB2DPi     = self._makeB2DPi(  name    = name,
                                     DSel    = self.selD,
                                     pionSel = self.selPions,
                                     config  = config)

    self.selB2TauMuSS = self._makeB2XMuSS(name    = name,
                                      tauSel  = self.selTau,
                                      muonSel = self.selMuons,
                                      config  = config)

    self.selB2DPiSS = self._makeB2DPiSS(name    = name,
                                   DSel    = self.selD,
                                   pionSel = self.selPions,
                                   config  = config)

    self.selB2DD0 = self._makeB2DD0(   name    = name,
                                       D0Sel  = self.selD0,
                                       DSel    = self.selD,
                                       config  = config)
    self.selPions_3pi3pi        = self._makePions_3pi3pi(      name    = "Pions_3pi3piFor"+name,
                                      config  = config)
    self.selTau_3pi3pi       = self._makeTau_3pi3pi( name    = "Tau_3pi3piFor"+name,
                                       pionSel = self.selPions_3pi3pi,
                                       config  = config)

    self.selB2TauMu_3pi = self._makeB2XMu_3pi(name    = name,
                                              tauSel  = self.selTau_3pi3pi,
                                              muonSel = self.selMuons,
                                              config  = config)

    self.selB2TauMu_3piSS = self._makeB2XMu_3piSS(name    = name,
                                      tauSel  = self.selTau_3pi3pi,
                                      muonSel = self.selMuons,
                                      config  = config)

    self.selB2TauTau_3pi3pi  = self._makeB2TauTau_3pi3pi( name   = name,
                                            tauSel = self.selTau_3pi3pi,
                                            config = config)

    self.selB2TauTau_3pi3piSS  = self._makeB2TauTau_3pi3piSS( name   = name,
                                            tauSel = self.selTau_3pi3pi,
                                            config = config)


    self.selB2TauTauTOS     = self._makeTOS(name+"_TOSForTauTau",self.selB2TauTau,config)
    self.selB2TauTauSSTOS   = self._makeTOS(name+"_TOSForTauTauSS",self.selB2TauTauSS,config)
    self.selB2TauMuTOS      = self._makeTOS(name+"_TOSForTauMu",self.selB2TauMu,config)
    self.selB2TauMuSSTOS    = self._makeTOS(name+"_TOSForTauMuSS",self.selB2TauMuSS,config)
    self.selB2TauTau_3pi3piTOS     = self._makeTOS(name+"_TOSForTauTau_3pi3pi",self.selB2TauTau_3pi3pi,config)
    self.selB2TauTau_3pi3piSSTOS     = self._makeTOS(name+"_TOSForTauTau_3pi3piSS",self.selB2TauTau_3pi3piSS,config)
    self.selB2TauMu_3piTOS     = self._makeTOS(name+"_TOSForTauMu_3pi",self.selB2TauMu_3pi,config)
    self.selB2TauMu_3piSSTOS     = self._makeTOS(name+"_TOSForTauMu_3piSS",self.selB2TauMu_3piSS,config)

    #
    # Define StrippingLines
    #
    # 1. TauTau_TOSLine
#    relatedInfoToolsConfig4TauTau_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauTauTOS,selTau,StdAllLoosePions])
    self.TauTau_TOSLine    = StrippingLine(name+"_TauTau_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauTau_TOSLinePrescale'],
                                           postscale   = config['B2TauTau_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "B0 -> ^tau+ tau-" : "TauVars_VertexIsoInfo_0",
                                                 "B0 -> tau+ ^tau-" : "TauVars_VertexIsoInfo_1"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "B0 -> ^tau+ tau-" : "TauVars_ConeIsolation_0",
                                                 "B0 -> tau+ ^tau-" : "TauVars_ConeIsolation_1"
                                               }
                                              }
                                            ],
                                           selection   = self.selB2TauTauTOS,
                                           MaxCandidates = 50
                                         )

    # 5. TauTau_SameSign_TOSLine
#    relatedInfoToolsConfig4TauTau_SS_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauTauSSTOS,selTau,StdAllLoosePions])
    self.TauTau_SS_TOSLine = StrippingLine(name+"_TauTau_SameSign_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauTau_SameSign_TOSLinePrescale'],
                                           postscale   = config['B2TauTau_SameSign_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ tau+]CC" : "TauVars_VertexIsoInfo_0",
                                                 "[B0 -> tau+ ^tau+]CC" : "TauVars_VertexIsoInfo_1"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ tau+]CC" : "TauVars_ConeIsolation_0",
                                                 "[B0 -> tau+ ^tau+]CC" : "TauVars_ConeIsolation_1"
                                               }
                                              }
                                           ],
                                           selection   = self.selB2TauTauSSTOS,
                                           MaxCandidates = 50
                                         )


    # 2. DD_Line
    daughter_locations_DD = {
      "B0 -> ^D+ D-" : "DVars_{0}_0",
      "B0 -> D+ ^D-" : "DVars_{0}_1",
      "B0 -> (D+ -> ^X+ K- pi+) (D- -> X- K+ pi-) ":"DVars_{0}_01",
      "B0 -> (D+ -> X+ ^K- pi+) (D- -> X- K+ pi-) ":"DVars_{0}_02",
      "B0 -> (D+ -> X+ K- ^pi+) (D- -> X- K+ pi-) ":"DVars_{0}_03",
      "B0 -> (D+ -> X+ K- pi+) (D- -> ^X- K+ pi-) ":"DVars_{0}_11",
      "B0 -> (D+ -> X+ K- pi+) (D- -> X- ^K+ pi-) ":"DVars_{0}_12",
      "B0 -> (D+ -> X+ K- pi+) (D- -> X- K+ ^pi-) ":"DVars_{0}_13",
    }
#    relatedInfoToolsConfig4DD_Line = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2DD,selD,selPionsForD,selKaonsForD])
    self.DD_Line        = StrippingLine(name+"_DD_Line",
                                        #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                        prescale    = config['B2DD_LinePrescale'],
                                        postscale   = config['B2DD_LinePostscale'],
                                        MDSTFlag = False,
                                        RelatedInfoTools = [
                                          { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                          { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                          { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                          { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                          { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                          { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                          { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                          { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                          { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                                    "DaughterLocations" : {
                                                      "B0 -> ^D+ D-" : "DVars_VertexIsoInfo_0",
                                                      "B0 -> D+ ^D-" : "DVars_VertexIsoInfo_1"
                                                    }
                                          },

                                          {'Type'             : 'RelInfoVertexIsolationBDT',
                                                     'Location'         : 'VertexIsoBDTInfo',
                                                     'DaughterLocations'   : {
                                                         "B0 -> ^D+ D-" : "DVars_VertexIsoBDTInfo_0",
                                                         "B0 -> D+ ^D-" : "DVars_VertexIsoBDTInfo_1"
                                                    }
                                                   },

                                          { "Type" : "RelInfoConeIsolation",
                                            "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                            "DaughterLocations" : {key: val.format('ConeIsolation') for key, val in daughter_locations_DD.items()}},

                                          { "Type" : "RelInfoConeVariables",
                                            "Location"  : "BVars_ConeVarsInfo05", "ConeAngle" : 0.5,
                                            "DaughterLocations" : {key: val.format('ConeVarsInfo') for key, val in daughter_locations_DD.items()}},

                                          {'Type'             : 'RelInfoTrackIsolationBDT',
                                                     'Variables' : 2,
                                                     'WeightsFile'  :  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
                                           "DaughterLocations" : {key: val.format('TrackIsoBDTInfo') for key, val in daughter_locations_DD.items()}},

                                    {'Type'              : 'RelInfoConeVariables',
                                     'ConeAngle'         : 1.0,
                                     'Location'          : 'ConeVarsInfo10'
                                     },
                                    {'Type'              : 'RelInfoConeVariables',
                                    'ConeAngle'         : 1.5,
                                     'Location'          : 'ConeVarsInfo15'
                                    },
                                    {'Type'              : 'RelInfoConeVariables',
                                     'ConeAngle'         : 2.0,
                                     'Location'          : 'ConeVarsInfo20'
                                    },
                                    {'Type'              : 'RelInfoConeIsolation',
                                     'ConeSize'         : 1.0,
                                     'Location'          : 'ConeIsoInfo10'
                                    },
                                    {'Type'              : 'RelInfoConeIsolation',
                                     'ConeSize'         : 1.5,
                                     'Location'          : 'ConeIsoInfo15'
                                            },
                                    {'Type'              : 'RelInfoConeIsolation',
                                     'ConeSize'         : 2.0,
                                     'Location'          : 'ConeIsoInfo20'
                                    },

                                        ],
                                        selection   = self.selB2DD,
                                        MaxCandidates = 50
                                      )

    # 9. DD0_Line
    daughter_locations_DD0 = {
      "[B+ -> ^(X0 -> K+ pi- ) (X+ -> X+ K- pi+)]CC":"D0Vars_{0}_0",
      "[B+ -> (X0 -> K+ pi- ) ^(X+ -> X+ K- pi+)]CC":"DVars_{0}_0",
      "[B+ -> (X0 -> ^K+ pi- ) (X+ -> X+ K- pi+)]CC":"D0Vars_{0}_01",
      "[B+ -> (X0 -> K+ ^pi- ) (X+ -> X+ K- pi+)]CC":"D0Vars_{0}_02",
      "[B+ -> (X0 -> K+ pi- ) (X+ -> ^X+ K- pi+)]CC":"DVars_{0}_01",
      "[B+ -> (X0 -> K+ pi- ) (X+ -> X+ ^K- pi+)]CC":"DVars_{0}_02",
      "[B+ -> (X0 -> K+ pi- ) (X+ -> X+ K- ^pi+)]CC":"DVars_{0}_03",
    }
    #    relatedInfoToolsConfig4DD_Line = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2DD,selD,selPionsForD,selKaonsForD])
    self.DD0_Line        = StrippingLine(name+"_DD0_Line",
                                         #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                         prescale    = config['B2DD0_LinePrescale'],
                                         postscale   = config['B2DD0_LinePostscale'],
                                         MDSTFlag = False,
                                         RelatedInfoTools = [
                                           { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                           { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                           { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                           { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                           { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                           { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                           { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                           { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                             "DaughterLocations" : {
                                               "[B+ -> ^D~0 D+]CC" : "DVars_VertexIsoInfo_0",
                                               "[B+ -> D~0 ^D+]CC" : "DVars_VertexIsoInfo_1"
                                             }
                                           },

                                           {'Type'             : 'RelInfoVertexIsolationBDT',
                                            'Location'         : 'VertexIsoBDTInfo',
                                            'DaughterLocations'   : {
                                              "[B+ -> ^D~0 D+]CC" : "DVars_VertexIsoBDTInfo_0",
                                              "[B+ -> D~0 ^D+]CC" : "DVars_VertexIsoBDTInfo_1"
                                            }
                                          },

                                           { "Type" : "RelInfoConeIsolation",
                                             "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                             "DaughterLocations" : {key: val.format('ConeIsolation') for key, val in daughter_locations_DD0.items()}},

                                           { "Type" : "RelInfoConeVariables",
                                             "Location"  : "BVars_ConeVarsInfo05", "ConeAngle" : 0.5,
                                             "DaughterLocations" : {key: val.format('ConeVarsInfo') for key, val in daughter_locations_DD0.items()}},

                                           {'Type'             : 'RelInfoTrackIsolationBDT',
                                            'Variables' : 2,
                                            'WeightsFile'  :  'BsMuMu_TrackIsolationBDT9vars_v1r4.xml',
                                            "DaughterLocations" : {key: val.format('TrackIsoBDTInfo') for key, val in daughter_locations_DD0.items()}},

                                           {'Type'              : 'RelInfoConeVariables',
                                            'ConeAngle'         : 1.0,
                                            'Location'          : 'ConeVarsInfo10'
                                          },
                                           {'Type'              : 'RelInfoConeVariables',
                                            'ConeAngle'         : 1.5,
                                            'Location'          : 'ConeVarsInfo15'
                                          },
                                           {'Type'              : 'RelInfoConeVariables',
                                            'ConeAngle'         : 2.0,
                                            'Location'          : 'ConeVarsInfo20'
                                          },
                                           {'Type'              : 'RelInfoConeIsolation',
                                            'ConeSize'         : 1.0,
                                            'Location'          : 'ConeIsoInfo10'
                                          },
                                           {'Type'              : 'RelInfoConeIsolation',
                                            'ConeSize'         : 1.5,
                                            'Location'          : 'ConeIsoInfo15'
                                          },
                                           {'Type'              : 'RelInfoConeIsolation',
                                            'ConeSize'         : 2.0,
                                            'Location'          : 'ConeIsoInfo20'
                                          },

                                         ],
                                         selection   = self.selB2DD0,
                                         MaxCandidates = 50
                                       )



    # 6. DD_SameSign_Line
 #   relatedInfoToolsConfig4DD_SS_Line = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2DDSS,selD,selPionsForD,selKaonsForD])
    self.DD_SS_Line     = StrippingLine(name+"_DD_SameSign_Line",
                                        #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                        prescale    = config['B2DD_SameSign_LinePrescale'],
                                        postscale   = config['B2DD_SameSign_LinePostscale'],
                                        MDSTFlag = False,
                                        RelatedInfoTools = [
                                          { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                          { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                          { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                          { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                          { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                          { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                          { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                          { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                          { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                                    "DaughterLocations" : {
                                                      "[B0 -> ^D+ D+]CC" : "DVars_VertexIsoInfo_0",
                                                      "[B0 -> D+ ^D+]CC" : "DVars_VertexIsoInfo_1"
                                                    }
                                          },
                                          { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                                    "DaughterLocations" : {
                                                      "[B0 -> ^D+ D+]CC" : "DVars_ConeIsolation_0",
                                                      "[B0 -> D+ ^D+]CC" : "DVars_ConeIsolation_1"
                                                    }
                                          }
                                        ],
                                        selection   = self.selB2DDSS,
                                        MaxCandidates = 50
                                       )

    # 3. TauMu_TOSLine
 #   relatedInfoToolsConfig4TauMu_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauMuTOS,selMuons,selTau,StdAllLoosePions])
    self.TauMu_TOSLine     = StrippingLine(name+"_TauMu_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauMu_TOSLinePrescale'],
                                           postscale   = config['B2TauMu_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu-]CC" : "TauVars_VertexIsoInfo_0"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu-]CC" : "TauVars_ConeIsolation",
                                                 "[B0 -> tau+ ^mu-]CC" : "MuVars_ConeIsolation"
                                               }
                                              }
                                           ],
                                           selection   = self.selB2TauMuTOS,
                                           MaxCandidates = 50
                                         )


    # 7. TauMu_SameSign_TOSLine
 #   relatedInfoToolsConfig4TauMu_SS_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauMuSSTOS,selMuons,selTau,StdAllLoosePions])
    self.TauMu_SS_TOSLine  = StrippingLine(name+"_TauMu_SameSign_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauMu_SameSign_TOSLinePrescale'],
                                           postscale   = config['B2TauMu_SameSign_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu+]CC" : "TauVars_VertexIsoInfo_0"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu+]CC" : "TauVars_ConeIsolation",
                                                 "[B0 -> tau+ ^mu+]CC" : "MuVars_ConeIsolation"
                                               }
                                              }
                                           ],
                                           selection   = self.selB2TauMuSSTOS,
                                           MaxCandidates = 50
                                          )

    # 4. DPi_Line
 #   relatedInfoToolsConfig4DPi_Line = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2DPi,selD,selPionsForD,selKaonsForD,selPions])
    self.DPi_Line       = StrippingLine(name+"_DPi_Line",
                                        #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                        prescale    = config['B2DPi_LinePrescale'],
                                        postscale   = config['B2DPi_LinePostscale'],
                                        MDSTFlag = False,
                                        RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^D+ pi-]CC" : "DVars_VertexIsoInfo_0"
                                               }
                                            },
                                            { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^D+ pi-]CC" : "TauVars_ConeIsolation",
                                                 "[B0 -> D+ ^pi-]CC" : "MuVars_ConeIsolation"
                                               }
                                            }
                                          ],
                                        selection   = self.selB2DPi,
                                        MaxCandidates = 50
                                     )

    # 8. DPi_SameSign_Line
#    relatedInfoToolsConfig4DPi_SS_Line = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2DPiSS,selD,selPionsForD,selKaonsForD,selPions])
    self.DPi_SS_Line    = StrippingLine(name+"_DPi_SameSign_Line",
                                        #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                        prescale    = config['B2DPi_SameSign_LinePrescale'],
                                        postscale   = config['B2DPi_SameSign_LinePostscale'],
                                        MDSTFlag = False,
                                        RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^D+ pi+]CC" : "DVars_VertexIsoInfo_0"
                                               }
                                            },
                                            { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^D+ pi+]CC" : "TauVars_ConeIsolation",
                                                 "[B0 -> D+ ^pi+]CC" : "MuVars_ConeIsolation"
                                               }
                                            }
                                           ],
                                        selection   = self.selB2DPiSS,
                                        MaxCandidates = 50
                                       )

  # 11. TauTau_3pi3piTOSLine
#    relatedInfoToolsConfig4TauTau_3pi3pi__TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauTau_3pi3piTOS,selTau_3pi3pi,StdAllLoosePions])
    self.TauTau_3pi3pi_TOSLine    = StrippingLine(name+"_TauTau_3pi3pi_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauTau_3pi3pi_TOSLinePrescale'],
                                           postscale   = config['B2TauTau_3pi3pi_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "B0 -> ^tau+ tau-" : "TauVars_VertexIsoInfo_0",
                                                 "B0 -> tau+ ^tau-" : "TauVars_VertexIsoInfo_1"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "B0 -> ^tau+ tau-" : "TauVars_ConeIsolation_0",
                                                 "B0 -> tau+ ^tau-" : "TauVars_ConeIsolation_1"
                                               }
                                              }
                                            ],
                                           selection   = self.selB2TauTau_3pi3piTOS,
                                           MaxCandidates = 50

                                          )


  # 12. TauTau_3pi3pi_SameSign_TOSLine
#    relatedInfoToolsConfig4TauTau_3pi3pi_SS_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauTau_3pi3piSSTOS,selTau_3pi3pi,StdAllLoosePions])
    self.TauTau_3pi3pi_SS_TOSLine = StrippingLine(name+"_TauTau_3pi3pi_SameSign_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauTau_3pi3pi_SameSign_TOSLinePrescale'],
                                           postscale   = config['B2TauTau_3pi3pi_SameSign_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ tau+]CC" : "TauVars_VertexIsoInfo_0",
                                                 "[B0 -> tau+ ^tau+]CC" : "TauVars_VertexIsoInfo_1"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ tau+]CC" : "TauVars_ConeIsolation_0",
                                                 "[B0 -> tau+ ^tau+]CC" : "TauVars_ConeIsolation_1"
                                               }
                                              }
                                           ],
                                           selection   = self.selB2TauTau_3pi3piSSTOS,
                                           MaxCandidates = 50
                                        )

     # 13. TauMu_3pi_TOSLine
 #   relatedInfoToolsConfig4TauMu_3pi_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauMu_3piTOS,selMuons,selTau_3pi3pi,StdAllLoosePions])
    self.TauMu_3pi_TOSLine     = StrippingLine(name+"_TauMu_3pi_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauMu_3pi_TOSLinePrescale'],
                                           postscale   = config['B2TauMu_3pi_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu-]CC" : "TauVars_VertexIsoInfo_0"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu-]CC" : "TauVars_ConeIsolation",
                                                 "[B0 -> tau+ ^mu-]CC" : "MuVars_ConeIsolation"
                                               }
                                              }
                                           ],
                                           selection   = self.selB2TauMu_3piTOS,
                                           MaxCandidates = 50
                                         )

  # 14. TauMu_3pi_SameSign_TOSLine
 #   relatedInfoToolsConfig4TauMu_3pi_SS_TOSLine = filter_relatedinfo_locations(config['RelatedInfoTools'],[selB2TauMu_3piSSTOS,selMuons,selTau_3pi3pi,StdAllLoosePions])
    self.TauMu_3pi_SS_TOSLine  = StrippingLine(name+"_TauMu_3pi_SameSign_TOSLine",
                                           #HLT         = " HLT_PASS_RE('"+HLT_DECISIONS+"') ",
                                           prescale    = config['B2TauMu_3pi_SameSign_TOSLinePrescale'],
                                           postscale   = config['B2TauMu_3pi_SameSign_TOSLinePostscale'],
                                           MDSTFlag = False,
                                           RelatedInfoTools = [
                                             { "Type" : "RelInfoBstautauMuonIsolationBDT",  "Location"  : "MuonIsolationBDT"},
                                             { "Type" : "RelInfoBstautauMuonIsolation" ,  "Location"  : "MuonIsolation" },
                                             { "Type" : "RelInfoBstautauTauIsolationBDT", "Location"  : "TauIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTauIsolation",  "Location"  : "TauIsolation"  },
                                             { "Type" : "RelInfoBstautauTrackIsolationBDT" ,  "Location"  : "TrackIsolationBDT" },
                                             { "Type" : "RelInfoBstautauTrackIsolation" , "Location"  : "TrackIsolation"},
                                             { "Type" : "RelInfoBstautauCDFIso" , "Location"  : "CDFIso"  },
                                             { "Type" : "RelInfoBstautauZVisoBDT" , "Location"  : "ZVisoBDT"},
                                             { "Type" : "RelInfoVertexIsolation", "Location" :  "BVars_VertexIsoInfo",
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu+]CC" : "TauVars_VertexIsoInfo_0"
                                               }
                                             },
                                             { "Type" : "RelInfoConeIsolation",  "Location"  : "BVars_ConeIsolation", "ConeSize" : 0.5,
                                               "DaughterLocations" : {
                                                 "[B0 -> ^tau+ mu+]CC" : "TauVars_ConeIsolation",
                                                 "[B0 -> tau+ ^mu+]CC" : "MuVars_ConeIsolation"
                                               }
                                              }
                                           ],
                                           selection   = self.selB2TauMu_3piSSTOS,
                                           MaxCandidates = 50
                                          )


    #
    # Register StrippingLines
    #
    self.registerLine( self.TauTau_TOSLine )        #  1
    self.registerLine( self.DD_Line )               #  2
    self.registerLine( self.TauMu_TOSLine )         #  3
    self.registerLine( self.DPi_Line )              #  4
    self.registerLine( self.TauTau_SS_TOSLine )     #  5
    self.registerLine( self.DD_SS_Line )            #  6
    self.registerLine( self.TauMu_SS_TOSLine )      #  7
    self.registerLine( self.DPi_SS_Line )           #  8
    self.registerLine( self.DD0_Line )              #  9
    self.registerLine( self.TauTau_3pi3pi_TOSLine )        # 11
    self.registerLine( self.TauTau_3pi3pi_SS_TOSLine )     # 12
    self.registerLine( self.TauMu_3pi_TOSLine )        # 13
    self.registerLine( self.TauMu_3pi_SS_TOSLine )     # 14


  #####################################################
  def _makeB2XX(self, name, tauSel, DSel, config):

    #
    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TT']       + "*MeV) & "\
               "(AM  > " + config['MASS_LOW_B']    + "*MeV) & "\
               "(AM  < " + config['MASS_HIGH_B']   + "*MeV)"

    _bcut    = "(VFASPF(VCHI2PDOF)  <   "   + config['VCHI2_B']       + ") & "\
               "(BPVDIRA            >   "   + config['DIRA_B']        + ") & "\
               "(BPVVDCHI2          >   "   + config['FDCHI2_B']      + ") & "\
               "(BPVVD          <   "   + config['FD_B']      + ") & "\
               "(PT                >   "   + config['PT_B_TT_HIGH']  + "*MeV) & "\
               "(INGENERATION((PT   >   "   + config['PT_B_TAU_CHILD_BEST']+ "*MeV),1)) & "\
               "(INGENERATION((PT   >   "   + config['PT_B_CHILD_BEST']+ "*MeV),2)) & "\
               "(sumpt >" + config['PT_B_PIONS_TOTAL']+ "*MeV) & "\
               "(max(CHILD(ipsm,1),CHILD(ipsm,2)) > "   + config['B_TAUPI_2NDMINIPS']+") & "\
               "(max(CHILD(MIPCHI2DV(PRIMARY),1),CHILD(MIPCHI2DV(PRIMARY),2)) > "   + config['IPCHI2_B_TAU_CHILD_BEST']+") &  "\
               "(min(CHILD(MIPCHI2DV(PRIMARY),1),CHILD(MIPCHI2DV(PRIMARY),2)) > "   + config['IPCHI2_B_TAU_CHILD_WORSE']+") & "\
               "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV))"


    _CombineTau = CombineParticles( DecayDescriptors = ["B0 -> tau+ tau-"],
                                   CombinationCut   = _combcut,
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)

    _CombineD   = CombineParticles( DecayDescriptors = ["B0 -> D+ D-"],
                                   CombinationCut   = _combcut + " & (AM > 5000)",
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)

    return (Selection(name+"_TauTau",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel ] ),
            Selection(name+"_DD",
                      Algorithm          = _CombineD,
                      RequiredSelections = [ DSel ] ))

#####################################################
  def _makeB2DD0(self, name, D0Sel, DSel, config):

    #
    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TT']       + "*MeV) & "\
               "(AM  > " + config['MASS_LOW_B']    + "*MeV) & "\
               "(AM  < " + config['MASS_HIGH_B']   + "*MeV)"

    _bcut    = "(VFASPF(VCHI2PDOF)  <   "   + config['VCHI2_B']       + ") & "\
               "(BPVDIRA            >   "   + config['DIRA_B']        + ") & "\
               "(BPVVDCHI2          >   "   + config['FDCHI2_B']      + ") & "\
               "(BPVVD          <   "   + config['FD_B']      + ") & "\
               "(PT                >   "   + config['PT_B_TT_HIGH']  + "*MeV) & "\
               "(INGENERATION((PT   >   "   + config['PT_B_TAU_CHILD_BEST']+ "*MeV),1)) & "\
               "(INGENERATION((PT   >   "   + config['PT_B_CHILD_BEST']+ "*MeV),2)) & "\
               "(sumpt >" + config['PT_B_PIONS_TOTAL']+ "*MeV) & "\
               "(max(CHILD(ipsm,1),CHILD(ipsm,2)) > "   + config['B_TAUPI_2NDMINIPS']+") & "\
               "(max(CHILD(MIPCHI2DV(PRIMARY),1),CHILD(MIPCHI2DV(PRIMARY),2)) > "   + config['IPCHI2_B_TAU_CHILD_BEST']+") &  "\
               "(min(CHILD(MIPCHI2DV(PRIMARY),1),CHILD(MIPCHI2DV(PRIMARY),2)) > "   + config['IPCHI2_B_TAU_CHILD_WORSE']+") & "\
               "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV))"



    _CombineD   = CombineParticles( DecayDescriptors = ["[B+ -> D~0 D+]cc"],
                                   CombinationCut   = _combcut + " & (AM > 5000)",
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)

    return Selection(name+"_DD0",
                     Algorithm          = _CombineD,
                     RequiredSelections = [ D0Sel, DSel ] )


  #####################################################
  def _makeB2XXSS(self, name, tauSel, DSel, config):

    #
    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID))",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TT']       + "*MeV) & "\
               "(AM  > " + config['MASS_LOW_B']    + "*MeV) & "\
               "(AM  < " + config['MASS_HIGH_B']   + "*MeV)"

    _bcut    = "(VFASPF(VCHI2PDOF)  <   "   + config['VCHI2_B']       + ") & "\
               "(BPVDIRA            >   "   + config['DIRA_B']        + ") & "\
               "(BPVVDCHI2          >   "   + config['FDCHI2_B']      + ") & "\
               "(BPVVD          <   "   + config['FD_B']      + ") & "\
               "(PT                >   "   + config['PT_B_TT_HIGH']  + "*MeV) & "\
               "(INGENERATION((PT   >   "   + config['PT_B_TAU_CHILD_BEST']+ "*MeV),1)) & "\
               "(INGENERATION((PT   >   "   + config['PT_B_CHILD_BEST']+ "*MeV),2)) & "\
               "(sumpt >" + config['PT_B_PIONS_TOTAL']+ "*MeV) & "\
               "(max(CHILD(ipsm,1),CHILD(ipsm,2)) > "   + config['B_TAUPI_2NDMINIPS']+") & "\
               "(max(CHILD(MIPCHI2DV(PRIMARY),1),CHILD(MIPCHI2DV(PRIMARY),2)) > "   + config['IPCHI2_B_TAU_CHILD_BEST']+") &  "\
               "(min(CHILD(MIPCHI2DV(PRIMARY),1),CHILD(MIPCHI2DV(PRIMARY),2)) > "   + config['IPCHI2_B_TAU_CHILD_WORSE']+") & "\
               "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV))"


    _CombineTau = CombineParticles( DecayDescriptors = ["[B0 -> tau+ tau+]cc"],
                                   CombinationCut   = _combcut,
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)

    _CombineD   = CombineParticles( DecayDescriptors = ["[B0 -> D+ D+]cc"],
                                   CombinationCut   = _combcut + " & (AM > 5000)",
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)

    return (Selection(name+"_TauTauSS",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel ] ),
            Selection(name+"_DDSS",
                      Algorithm          = _CombineD,
                      RequiredSelections = [ DSel ] ))

  #####################################################
  def _makeB2XMu(self, name, tauSel, muonSel,  config):

    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TM']       + "*MeV) & "\
      "(AM  > "          + config['MASS_LOW_B']    + "*MeV) & "\
      "(AM  < "          + config['MASS_HIGH_B']   + "*MeV)"

    _bcut    = "((CHILD(BPVVDCHI2,1))   <   "   + config['FDCHI2_TAU']       + ") & "\
      "(MIPCHI2DV(PRIMARY)   <              "   + config['IPCHI2_B_MU']        + ") & "\
      "(BPVVD          <   "   + config['FD_B_MU']      + ") & "\
      "(PT                >   "   + config['PT_B_TM_HIGH']  + "*MeV) & "\
      "(sumpt >" + config['PT_B_MU_PIONS_TOTAL']+ "*MeV) & "\
      "((CHILD(VFASPF(VCHI2),1))   <   "   + config['VCHI2_B_TAU_MU']       + ") & "\
      "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV)) &"\
      "((CHILD(MIPCHI2DV(PRIMARY),1)) > "   + config['IPCHI2_B_TAU_MU']+")   "


    _CombineTau = CombineParticles( DecayDescriptors = ["[B0 -> tau+ mu-]cc"],
                                   CombinationCut   = _combcut,
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)


    return (Selection(name+"_TauMu",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel, muonSel ] ))

    #####################################################
  def _makeB2XMuSS(self, name, tauSel, muonSel, config):

    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TM']       + "*MeV) & "\
      "(AM  > "          + config['MASS_LOW_B']    + "*MeV) & "\
      "(AM  < "          + config['MASS_HIGH_B']   + "*MeV)"

    _bcut    = "((CHILD(BPVVDCHI2,1))   <   "   + config['FDCHI2_TAU']       + ") & "\
      "(MIPCHI2DV(PRIMARY)   <              "   + config['IPCHI2_B_MU']        + ") & "\
      "(BPVVD          <   "   + config['FD_B_MU']      + ") & "\
      "(PT                >   "   + config['PT_B_TM_HIGH']  + "*MeV) & "\
      "(sumpt >" + config['PT_B_MU_PIONS_TOTAL']+ "*MeV) & "\
      "((CHILD(VFASPF(VCHI2),1))   <   "   + config['VCHI2_B_TAU_MU']       + ") & "\
      "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV)) &"\
      "((CHILD(MIPCHI2DV(PRIMARY),1)) > "   + config['IPCHI2_B_TAU_MU']+")   "


    _CombineTau = CombineParticles( DecayDescriptors = ["[B0 -> tau+ mu+]cc"],
                                   CombinationCut   = _combcut,
                                   MotherCut        = _bcut,
                                   Preambulo        = preambulo)


    return (Selection(name+"_TauMuSS",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel, muonSel ] ))
  #####################################################
  def _makeB2DPi(self, name, DSel, pionSel, config):

    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID))",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TM']       + "*MeV) & "\
      "(AM  > "          + config['MASS_LOW_B']    + "*MeV) & "\
      "(AM  < "          + config['MASS_HIGH_B']   + "*MeV)"

    _bcut    = "((CHILD(BPVVDCHI2,1))   <   "   + config['FDCHI2_TAU']       + ") & "\
      "(MIPCHI2DV(PRIMARY)   <              "   + config['IPCHI2_B_MU']        + ") & "\
      "(BPVVD          <   "   + config['FD_B_MU']      + ") & "\
      "(PT                >   "   + config['PT_B_TM_HIGH']  + "*MeV) & "\
      "(sumpt >" + config['PT_B_MU_PIONS_TOTAL']+ "*MeV) & "\
      "((CHILD(VFASPF(VCHI2),1))   <   "   + config['VCHI2_B_TAU_MU']       + ") & "\
      "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV)) &"\
      "((CHILD(MIPCHI2DV(PRIMARY),1)) > "   + config['IPCHI2_B_TAU_MU']+")   "

    _CombineD   = CombineParticles( DecayDescriptors = ["[B0 -> D+ pi-]cc"],
                                    CombinationCut   = _combcut + " & (AM > 5000)",
                                    MotherCut        = _bcut,
                                    Preambulo        = preambulo)

    return (
      Selection(name+"_DPi",
                Algorithm          = _CombineD,
                RequiredSelections = [ DSel, pionSel ] ))


  #####################################################
  def _makeB2DPiSS(self, name, DSel, pionSel, config):

    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(APT > " + config['PT_B_TM']       + "*MeV) & "\
      "(AM  > "          + config['MASS_LOW_B']    + "*MeV) & "\
      "(AM  < "          + config['MASS_HIGH_B']   + "*MeV)"


    _bcut    = "((CHILD(BPVVDCHI2,1))   <   "   + config['FDCHI2_TAU']       + ") & "\
      "(MIPCHI2DV(PRIMARY)   <              "   + config['IPCHI2_B_MU']        + ") & "\
      "(BPVVD          <   "   + config['FD_B_MU']      + ") & "\
      "(PT                >   "   + config['PT_B_TM_HIGH']  + "*MeV) & "\
      "(in_range("+config['MCOR_LOW_B']+"*MeV,MCOR,"+config['MCOR_HIGH_B']+"*MeV)) &"\
      "((CHILD(MIPCHI2DV(PRIMARY),1)) > "   + config['IPCHI2_B_TAU_MU']+")   "


    _CombineD   = CombineParticles( DecayDescriptors = ["[B0 -> D+ pi+]cc"],
                                    CombinationCut   = _combcut + " & (AM > 5000)",
                                    MotherCut        = _bcut,
                                    Preambulo        = preambulo)

    return (
      Selection(name+"_DPiSS",
                Algorithm          = _CombineD,
                RequiredSelections = [ DSel, pionSel ] ))

  #####################################################
  def _filterTau(self, name, tauInput, config) :
    """
      Tau filter
      """
    _cut =       "(PT > 0*MeV) "

    _preambulo = [ "c1c2c3 = ((('pi+') == ABSID) | (('K+') == ABSID))" ,
                   "ipsm    = MINTREE( c1c2c3 , MIPCHI2DV(PRIMARY) )"]

    _filter   = FilterDesktop( Code = _cut, Preambulo = _preambulo)
#    _filter   = FilterDesktop( Code = _cut)

    return Selection(name,
                     Algorithm = _filter,
                     RequiredSelections = [tauInput]
                     )

  #####################################################
  def _makeD(self, name, pionSel, kaonSel, config) :

     _preambulo = [ "c1c2c3 = ((('pi+') == ABSID) | (('K+') == ABSID))" ,
                    "ipsm    = MINTREE( c1c2c3 , MIPCHI2DV(PRIMARY) )"]

#     _combcut = " (((AM  > "           + config['MASS_LOW_D']    + "*MeV) & "\
#                " (AM  < "           + config['MASS_HIGH_D']   + "*MeV))  | "\
#                " ((AM  > "           + config['MASS_LOW_Ds']    + "*MeV) & "\
#                " (AM  < "           + config['MASS_HIGH_Ds']   + "*MeV)))  & "\
#                " (APT  > "          + config['APT_D']   + "*MeV)  & "\
#                " (AMAXDOCA('')  <"  + config['AMAXDOCA_D']   + "*mm)  & "\
#                " (ANUM(PT > "       + config['MaxPT_D']   + "*MeV) >= 1) "

#     _bcut = " (PT > "             + config['PT_D']   + "*MeV) &"\
#             " (BPVDIRA >"         + config['DIRA_D']   + ") &"\
#             " (VFASPF(VCHI2) < "  + config['VCHI2_D']   + ")&"\
#             " (BPVVDCHI2 > "      + config['FDCHI2_D']   + ") &"\
#             " (BPVVDRHO >  "      + config['VDRHOmin_D']   + "*mm) &"\
#             " (BPVVDRHO <  "      + config['VDRHOmax_D']   + "*mm) &"\
#             " (BPVVDZ >  "        + config['VDZ_D']   + "*mm) "


#     _CombineD   = CombineParticles( DecayDescriptors = ["[D+ -> pi+ K- pi+]cc","[D+ -> K+ K- pi+]cc"],
#                                     CombinationCut   = _combcut,
#                                     MotherCut        = _bcut+"& (M>1800.*MeV) & (M<2030.*MeV)" ,
#                                     Preambulo        = _preambulo)

     _CombineD   = DaVinci__N3BodyDecays(
                   DecayDescriptors = ["[D+ -> pi+ K- pi+]cc","[D+ -> K+ K- pi+]cc"],
                   Preambulo        = _preambulo,
                   Combination12Cut = "AM<5000",
                   CombinationCut = " ( ( in_range ( %(MASS_LOW_D)s, AM, %(MASS_HIGH_D)s ) ) | ( in_range ( %(MASS_LOW_Ds)s, AM, %(MASS_HIGH_Ds)s ) ) )"\
                                    "& ( APT           > %(APT_D)s )"\
                                    "& ( AMAXDOCA('')  < %(AMAXDOCA_D)s )"\
                                    "& ( ANUM(PT       > %(MaxPT_D)s)>=1 )" %config,
                   MotherCut =   "( PT            > %(PT_D)s )"\
                                 "& ( BPVDIRA       > %(DIRA_D)s )"\
                                 "& ( VFASPF(VCHI2) < %(VCHI2_D)s ) "\
                                 "& ( BPVVDCHI2     > %(FDCHI2_D)s ) & ( in_range ( %(VDRHOmin_D)s, BPVVDRHO, %(VDRHOmax_D)s ) ) & ( BPVVDZ > %(VDZ_D)s )"\
                                 "& ( in_range ( %(MASS_LOW_Dmother)s, M, %(MASS_HIGH_Dmother)s ) )" %config
                   )
     return Selection(name,
                      Algorithm          = _CombineD,
                      RequiredSelections = [pionSel, kaonSel]
                      )


#####################################################
  def _makeD0(self, name, pionSel, kaonSel, config) :

     _preambulo = [ "c1c2c3 = ((('pi+') == ABSID) | (('K+') == ABSID))" ,
                    "ipsm    = MINTREE( c1c2c3 , MIPCHI2DV(PRIMARY) )"]

     _CombineD   = CombineParticles(
                   DecayDescriptors = ["[D0 -> K- pi+]cc"],
                   Preambulo        = _preambulo,
                   CombinationCut = " ( ( in_range ( %(MASS_LOW_D)s, AM, %(MASS_HIGH_D)s ) ) | ( in_range ( %(MASS_LOW_Ds)s, AM, %(MASS_HIGH_Ds)s ) ) )"\
                                    "& ( APT           > %(APT_D)s )"\
                                    "& ( AMAXDOCA('')  < %(AMAXDOCA_D)s )"\
                                    "& ( ANUM(PT       > %(MaxPT_D)s)>=1 )" %config,
                   MotherCut =   "( PT            > %(PT_D)s )"\
                                 "& ( BPVDIRA       > %(DIRA_D)s )"\
                                 "& ( VFASPF(VCHI2) < %(VCHI2_D)s ) "\
                                 "& ( BPVVDCHI2     > %(FDCHI2_D)s ) & ( in_range ( %(VDRHOmin_D)s, BPVVDRHO, %(VDRHOmax_D)s ) ) & ( BPVVDZ > %(VDZ_D)s )"\
                                 "& ( in_range ( %(MASS_LOW_Dmother)s, M, %(MASS_HIGH_Dmother)s ) )" %config
                   )
     return Selection(name,
                      Algorithm          = _CombineD,
                      RequiredSelections = [pionSel, kaonSel]
                      )

  #####################################################
  def _makePions(self, name, config) :
    """
      Pion selection for B -> D pi
      """
    _code = self._muFinalStateKinematicCuts(config)

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm          = _Filter,
                     RequiredSelections = [ StdLoosePions ] )
  #####################################################
  def _makePionsForD(self, name, config) :
    """
      Pion selection for B -> DD
      """
    _code = self._hadFinalStateKinematicCuts(config) +" & (PROBNNpi > 0.55)"

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm          = _Filter,
                     RequiredSelections = [ StdLoosePions ] )
  #####################################################
  def _makeKaonsForD(self, name, config) :
    """
      Kaon selection for B -> DD
      """
    _code = self._hadFinalStateKinematicCuts(config)

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm          = _Filter,
                     RequiredSelections = [ StdLooseKaons ] )

  #####################################################
  def _makeMuons(self, name, config) :
    """
      Muon selection
      """
    _code = self._muFinalStateKinematicCuts(config) +" & (HASMUON) & (PIDmu > 0)"

    _Filter = FilterDesktop(Code = _code)

    return Selection(name,
                     Algorithm          = _Filter,
                     RequiredSelections = [ StdTightMuons ] )

  #####################################################
  def _hadFinalStateKinematicCuts(self, config) :
    _code = "(P  > "                    + config['P_HAD_ALL_FINAL_STATE']     + "*MeV) & "\
      "(PT > "                    + config['PT_HAD_ALL_FINAL_STATE']          + "*MeV) & "\
      "(MIPCHI2DV(PRIMARY) > "    + config['IPCHI2_HAD_ALL_FINAL_STATE']      + ")     & "\
      "(TRCHI2DOF < "             + config['TRACKCHI2_HAD_ALL_FINAL_STATE']   + ")     & "\
      "(TRGHOSTPROB < "           + config['TRGHOPROB_HAD_ALL_FINAL_STATE']   + ")     "

    return _code

  #####################################################
  def _muFinalStateKinematicCuts(self, config) :
    _code = "(P  > "                    + config['P_MU']           + "*MeV) & "\
      "(PT > "                    + config['PT_MU']          + "*MeV) & "\
      "(MIPCHI2DV(PRIMARY) > "    + config['IPCHI2_MU']      + ")     & "\
      "(TRCHI2DOF < "             + config['TRACKCHI2_MU']   + ")     & "\
      "(TRGHOSTPROB < "           + config['TRGHOPROB_MU']   + ")      "

    return _code
   #####################################################
  def _makeB2XMu_3pi(self, name, tauSel, muonSel, config):

    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(AM  > 2000 *MeV) & "\
               "(AM  < 7000 *MeV)"
    _bcut      = "((CHILD(MIPCHI2DV(PRIMARY),1)) > "   + config['IPCHI2_B_TAU_MU']+")  &  "\
                 "(P > 25000) & (PT > 2000) & " \
                 "(M > " + config['CUTBASED_M_B'] + ") & "\
                 "(MCOR < " + config['CUTBASED_MCORR_B_HIGH'] + ") & "\
                 "(MCOR > " + config['CUTBASED_MCORR_B_LOW'] + ") & "\
                 "(M - CHILD(M,1) - CHILD(M,2) > " + config['CUTBASED_MISS_MASS'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + ") "

    _CombineTau = CombineParticles( DecayDescriptors = ["[B0 -> tau+ mu-]cc"],
                                    CombinationCut   = _combcut,
                                    MotherCut        = _bcut,
                                    Preambulo        = preambulo)


    return Selection(name+"_TauMu_3pi",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel, muonSel ] )




  ##################################################
  def _makeB2TauTau_3pi3pi(self, name, tauSel, config):
    # Minimum mass = 6 pions (140 MeV) = 840 MeV
    # TauDECAYTIME: c tau = m Delta/p
    _preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR  = sqrt(M**2 + PTRANS**2) + PTRANS"]
    #             "TauDECAYTIME = lambda i : (MASS(i) * MeV * (CHILDFUN(i, VFASPF(VZ)) - VFASPF(VZ))/(CHILDFUN(i, PZ) * 0.299792458))"] # lots of functions not defined
    _combcut   = "(AM > 2000) & (AM < 7000)"
    _bcut      = "(P > 25000) & (PT > 2000) & " \
                 "(M > " + config['CUTBASED_M_B'] + ") & "\
                 "(MCOR < " + config['CUTBASED_MCORR_B_HIGH'] + ") & "\
                 "(MCOR > " + config['CUTBASED_MCORR_B_LOW'] + ") & "\
                 "(M - CHILD(M,1) - CHILD(M,2) > " + config['CUTBASED_MISS_MASS'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + ") & "\
                 "(CHILD(M,2) * (CHILD(VFASPF(VZ),2) - VFASPF(VZ) )/(CHILD(PZ,2) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + ") & "\
                 "(CHILD(M,2) * (CHILD(VFASPF(VZ),2) - VFASPF(VZ) )/(CHILD(PZ,2) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + ")"
#             "(TauDECAYTIME(1) > " + config['CUTBASED_TAU_TAU'] + "*ps) & #"\
    #             "(TauDECAYTIME(2) > " + config['CUTBASED_TAU_TAU'] + "*ps)"


    _CombineTau = CombineParticles( DecayDescriptors = ["B0 -> tau+ tau-"],
                                    CombinationCut   = _combcut,
                                    MotherCut        = _bcut,
                                    Preambulo        = _preambulo)

    return Selection( name+"_TauTau_3pi3pi",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel ]
                      )

 #####################################################
  def _makeB2XMu_3piSS(self, name, tauSel, muonSel, config):

    preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR = sqrt(M**2 + PTRANS**2) + PTRANS",
                 "allpi = ((('pi+') == ABSID) | (('K+') == ABSID)) ",
                 "sumpt    = SUMTREE( allpi , PT )"]

    _combcut = "(AM  > 2000 *MeV) & "\
               "(AM  < 7000 *MeV)"

    _bcut      = "((CHILD(MIPCHI2DV(PRIMARY),1)) > "   + config['IPCHI2_B_TAU_MU']+")  &  "\
                 "(P > 25000) & (PT > 2000) & " \
                 "(M > " + config['CUTBASED_M_B'] + ") & "\
                 "(MCOR < " + config['CUTBASED_MCORR_B_HIGH'] + ") & "\
                 "(MCOR > " + config['CUTBASED_MCORR_B_LOW'] + ") & "\
                 "(M - CHILD(M,1) - CHILD(M,2) > " + config['CUTBASED_MISS_MASS'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + ") "

    _CombineTau = CombineParticles( DecayDescriptors = ["[B0 -> tau+ mu+]cc"],
                                    CombinationCut   = _combcut,
                                    MotherCut        = _bcut,
                                    Preambulo        = preambulo)

    return Selection(name+"_TauMu_3piSS",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel, muonSel ] )

    ##################################################
  def _makeB2TauTau_3pi3piSS(self, name, tauSel, config):
    # Minimum mass = 6 pions (140 MeV) = 840 MeV
    # TauDECAYTIME: c tau = m Delta/p
    _preambulo = ["PTRANS = P*sqrt( 1-BPVDIRA**2 )",
                 "MCOR  = sqrt(M**2 + PTRANS**2) + PTRANS"]
    #             "TauDECAYTIME = lambda i : (MASS(i) * MeV * (CHILDFUN(i, VFASPF(VZ)) - VFASPF(VZ))/(CHILDFUN(i, PZ) * 0.299792458))"] # lots of functions not defined
    _combcut   = "(AM > 2000) & (AM < 7000)"
    _bcut      = "(P > 25000) & (PT > 2000) & " \
                 "(M > " + config['CUTBASED_M_B'] + ") & "\
                 "(MCOR < " + config['CUTBASED_MCORR_B_HIGH'] + ") & "\
                 "(MCOR > " + config['CUTBASED_MCORR_B_LOW'] + ") & "\
                 "(M - CHILD(M,1) - CHILD(M,2) > " + config['CUTBASED_MISS_MASS'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + ") & "\
                 "(CHILD(M,2) * (CHILD(VFASPF(VZ),2) - VFASPF(VZ) )/(CHILD(PZ,2) * 0.299792458) < " + config['CUTBASED_TAU_TAU_HIGH'] + ") & "\
                 "(CHILD(M,1) * (CHILD(VFASPF(VZ),1) - VFASPF(VZ) )/(CHILD(PZ,1) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + ") & "\
                 "(CHILD(M,2) * (CHILD(VFASPF(VZ),2) - VFASPF(VZ) )/(CHILD(PZ,2) * 0.299792458) > " + config['CUTBASED_TAU_TAU_LOW'] + ")"
#             "(TauDECAYTIME(1) > " + config['CUTBASED_TAU_TAU'] + "*ps) & #"\
    #             "(TauDECAYTIME(2) > " + config['CUTBASED_TAU_TAU'] + "*ps)"


    _CombineTau = CombineParticles( DecayDescriptors = ["[B0 -> tau+ tau+]cc"],
                                    CombinationCut   = _combcut,
                                    MotherCut        = _bcut,
                                    Preambulo        = _preambulo)

    return Selection( name+"_TauTau_3pi3piSS",
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ tauSel ]
                      )


 ##################################################
  def _makeTau_3pi3pi(self, name, pionSel, config) :

    # Minimum mass = 3 pions (140 MeV) = 420 MeV
    _preambulo = [""]
    _combcut   = "(AM > " + config['CUTBASED_M_TAU_LOW'] + ") & (AM < " + config['CUTBASED_M_TAU_HIGH'] + ")"
    _bcut      = "(P > 7500) & (PT > 500) & "\
                 "(BPVVDRHO > " + config['CUTBASED_TAU_BPVVDRHO'] + ") & "\
                 "(VFASPF(VCHI2) < " + config['CUTBASED_VCHI2_TAU'] +")"

    _CombineTau = CombineParticles( DecayDescriptors = ["[tau+ -> pi+ pi- pi+]cc"],
                                    CombinationCut   = _combcut,
                                    MotherCut        = _bcut,
                                    Preambulo        = _preambulo)

    return Selection( name,
                      Algorithm          = _CombineTau,
                      RequiredSelections = [ pionSel ]
                      )
   ##################################################
  def _makePions_3pi3pi(self, name, config) :

    _code = self._hadFinalStateKinematicCuts_3pi3pi(config) + "  & (PROBNNghost < 0.4) & (PROBNNpi > " + config['CUTBASED_PROBNNPI'] + ")"
    _Filter = FilterDesktop(Code = _code)

    return Selection( name,
                      Algorithm          = _Filter,
                      RequiredSelections = [ StdNoPIDsPions ]
                      )

  ##################################################

  def _hadFinalStateKinematicCuts_3pi3pi(self, config) :
    # Reapply generator level cuts + default track cuts
    _code = "(P  > "                    + config['P_HAD_ALL_FINAL_STATE']     + "*MeV) & "\
      "(PT > "                    + config['PT_HAD_ALL_FINAL_STATE']          + "*MeV) & "\
      "(TRCHI2DOF < "             + config['TRACKCHI2_HAD_ALL_FINAL_STATE']   + ")     & "\
      "(TRGHOSTPROB < "           + config['TRGHOPROB_HAD_ALL_FINAL_STATE']   + ")     "

    return _code



  #####################################################
  # TISTOSSING
  #####################################################

  def _makeTISTOSFilter(self,name,specs):
    from Configurables import TisTosParticleTagger
    tisTosFilter = TisTosParticleTagger(name+'TISTOSFilter')
    tisTosFilter.TisTosSpecs = specs
    tisTosFilter.ProjectTracksToCalo = False
    tisTosFilter.CaloClustForCharged = False
    tisTosFilter.CaloClustForNeutral = False
    tisTosFilter.TOSFrac = {4:0.0, 5:0.0}
    # tisTosFilter = True
    return tisTosFilter

  def _makeTOS(self, name, sel, config):
    ''' TOS filters selections'''
    if ((name.find('TauMu') > -1) or (name.find('DMu') > -1)):
      tisTosFilter = self._makeTISTOSFilter(name,config['HLT_DECISIONS_MUON'])
    else :
      tisTosFilter = self._makeTISTOSFilter(name,config['HLT_DECISIONS_HAD'])
    return Selection(name, Algorithm=tisTosFilter, RequiredSelections=[sel])
