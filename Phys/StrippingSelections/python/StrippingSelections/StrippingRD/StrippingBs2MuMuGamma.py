"""
Stripping options for (pre-)selecting Bs -> mu mu gamma

Authors: Meril Reboud, Jean-Francois Marchand
"""

########################################################################
__author__ = ['Meril Reboud', 'Jean-Francois Marchand']
__date__ = '10/05/2017'


__all__ = ('Bs2MuMuGammaConf'
           'makeBs2MuMuGamma',
           'default_config',
           )

default_config = {
    'NAME'        : 'Bs2MuMuGamma',
    'WGs'         : ['RD'],
    'BUILDERTYPE' : 'Bs2MuMuGammaConf',
    'CONFIG'      : {'Prescale'			: 1,
                     'GammaPT'			: 500,
                     'GammaE'			: 1500,
                     'GammaCL'			: 0.2,
                     'TrChi2'			: 3,
                     'TrGhostProb'		: 0.4,
                     'TrIPChi2'			: 9,
                     'DOCAmu'			: 0.5,
                     'BPT'			: 350,
                     'BIPChi2'			: 20,
                     'MassWindow'	        : 1500,
                     'CombMass'			: 7000,
                     'CombPT'			: 500,
                     'VCHI2_VDOF'		: 15,
                     'DIRA'			: 0.995
                     },
    'STREAMS'     : ['Leptonic']
    }


#######################
#######################

from Gaudi.Configuration	import *

from StandardParticles		import StdLooseMuons, StdLooseAllPhotons

from PhysSelPython.Wrappers		import Combine3BodySelection
from StrippingConf.StrippingLine	import StrippingLine
from StrippingUtils.Utils		import LineBuilder, checkConfig



class Bs2MuMuGammaConf( LineBuilder ) :
    """Class defining the Bs -> mu mu gamma stripping line"""
    

    __configuration_keys__ = ('Prescale',
                              'GammaPT',
                              'GammaE',
                              'GammaCL',
                              'TrChi2',
                              'TrGhostProb',
                              'TrIPChi2',
                              'DOCAmu',
                              'BPT',
                              'BIPChi2',
                              'MassWindow',
                              'CombMass',
                              'CombPT',
                              'VCHI2_VDOF',
                              'DIRA'
                              )

    def __init__( self, name, config ):        
        
        LineBuilder.__init__(self, name, config)
        
        # make the various stripping selections
        self.selBs2MuMuGamma = makeBs2MuMuGamma(name, config)
        
        self.lineBs2MuMuGamma = StrippingLine( name+"Line",
                                               prescale  = config['Prescale'],
                                               selection = self.selBs2MuMuGamma,
                                               #RequiredRawEvents = ["Trigger","Muon","Calo","Rich","Velo","Tracker"]
                                               RelatedInfoTools = [

#### Photon Veto Variables:
                {"Type": "RelInfoGammaIso",
                 "DaughterLocations": {"B_s0 -> mu+ mu- ^gamma": "GammaIsolation"}},

#### Photon Cone Iso Variables:
                {"Type": "RelInfoConeVariables",
                 "ConeAngle": 1.0,
                 "IgnoreUnmatchedDescriptors": True,
                 "DaughterLocations": {
                        "Beauty -> l+  l- ^gamma": "GammaConeIsolation",
                        }
                 },

#### Old Bs2MuMu isolation Variables:
                {"Type": "RelInfoBs2MuMuBIsolations",
                 "Variables" : ['BSMUMUCDFISO', 'BSMUMUOTHERBMAG', 'BSMUMUOTHERBANGLE', 'BSMUMUOTHERBBOOSTMAG', 'BSMUMUOTHERBBOOSTANGLE', 'BSMUMUOTHERBTRACKS', 'BSMUMUPARTID','BSMUMUTOPID'],
                 "Location"  : "BSMUMUVARIABLES"},

#### Track Bs2MuMu2014 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.
                {"Type": "RelInfoTrackIsolationBDT",
                 "Variables": 2, #Set A+C 2014
                 "DaughterLocations": {
                        "B_s0 -> ^mu+ mu- gamma": "Muon1TrackIsoBDTInfo_Old",
                        "B_s0 -> mu+ ^mu- gamma": "Muon2TrackIsoBDTInfo_Old"
                        },
                 "WeightsFile":  "BsMuMu_TrackIsolationBDT9vars_v1r4.xml"},

#### Long Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.                
                {"Type": "RelInfoTrackIsolationBDT",
                 "Variables": -1, #Set A+C 2017
                 "DaughterLocations": {
                        "B_s0 -> ^mu+ mu- gamma": "Muon1TrackIsoBDTInfo",
                        "B_s0 -> mu+ ^mu- gamma": "Muon2TrackIsoBDTInfo"
                        },
                 "WeightsFile":  "BsMuMu_TrackIsolationBDT9vars_v2.xml"},

#### Velo Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.                
                {"Type": "RelInfoTrackIsolationBDT",
                 "Variables": -2, #Set A 2017
                 "DaughterLocations": {
                        "B_s0 -> ^mu+ mu- gamma": "Muon1VeloTrackIsoBDTInfo",
                        "B_s0 -> mu+ ^mu- gamma": "Muon2VeloTrackIsoBDTInfo"
                        },
                 "TrackType": 1,
                 "WeightsFile":  "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"},
                ]

                                               )
        
        self.registerLine(self.lineBs2MuMuGamma)
        
def makeBs2MuMuGamma(name, config):

    _Muon_cuts = "(MIPCHI2DV(PRIMARY) > %(TrIPChi2)s) "\
        "& (TRCHI2DOF < %(TrChi2)s) "\
        "& (TRGHOSTPROB < %(TrGhostProb)s)" % config
    
    _Gamma_cuts = "(PT > %(GammaPT)s*MeV) "\
        "& (E > %(GammaE)s*MeV) "\
        "& (CL > %(GammaCL)s)" % config

    _Comb12_cuts = "((AM < 3033*MeV) | (AM > 3916*MeV)) "\
        "& (AM < %(CombMass)s*MeV) "\
        "& (AMAXDOCA('') < %(DOCAmu)s*mm)" % config

    _Comb_cuts = "(AM < %(CombMass)s*MeV) "\
        "& (APT > %(CombPT)s*MeV)" % config

    _Mother_cuts = "(VFASPF(VCHI2/VDOF) < %(VCHI2_VDOF)s) "\
        "& (ADMASS('B_s0') < %(MassWindow)s*MeV) "\
        "& (BPVDIRA > %(DIRA)s) "\
        "& (PT > %(BPT)s*MeV) "\
        "& (BPVIPCHI2() < %(BIPChi2)s)" % config

    
    return Combine3BodySelection(name,
                                 [StdLooseMuons, StdLooseAllPhotons],
                                 DecayDescriptor = "B_s0 -> mu+ mu- gamma",
                                 DaughtersCuts = {"mu-":	_Muon_cuts, 
                                                  "mu+":	_Muon_cuts,
                                                  "gamma":	_Gamma_cuts
                                                  },
                                 Combination12Cut = _Comb12_cuts,
                                 CombinationCut = _Comb_cuts,
                                 MotherCut = _Mother_cuts, 
                                 )



########################################################################  
