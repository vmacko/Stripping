#!/bin/env python

import subprocess, os, sys, copy, re

class SumDict(dict) :
    '''Dict that can be added to another dict, assuming all its values support addition.'''
    def __add__(self, other) :
        newdict = self.__class__()
        for key, val in other.iteritems() :
            newdict[key] = val
        for key, val in self.iteritems() :
            if key in newdict :
                newdict[key] = newdict[key] + val
            else :
                newdict[key] = val
        return newdict

class DiskUsage(SumDict) :
    '''DST disk usage summary.'''

    def __init__(self, dirname = None) :
        if not dirname :
            return 
        elif isinstance(dirname, self.__class__) :
            SumDict.__init__(dirname)
            return 
        cmd = 'du -b ' + os.path.join(dirname, '*dst')
        duproc = subprocess.Popen(cmd,
                                  shell = True, stdout = subprocess.PIPE)
        stdout, stderr = duproc.communicate()
        if 0 != duproc.poll() :
            raise OSError('Failed to call ' + cmd)
        duresults = {}
        for line in filter(None, stdout.split('\n')) :
            line = line.split()
            duresults[line[1]] = float(line[0])
        duproc.stdout.close()
        full = SumDict()
        wgs = SumDict()
        lines = SumDict()
        merged = SumDict()
        for fname, du in duresults.iteritems() :
            fname = os.path.split(fname)[1]
            if 'Line_' in fname :
                # dsts are formatted 00000.Line_[M]?DST_<linename>.[m]?dst
                linename = fname.split('_')[-1]
                lines += SumDict({linename.split('.')[-1] : SumDict({linename : du})})
            elif 'WG_' in fname :
                # dsts are formatted 00000.WG_<wg>_<stream>.[m]?dst
                wg, stream = fname.split('_')[-2:]
                wgs += SumDict({wg : SumDict({stream : du})})
            else :
                # dsts are formated 00000.<stream>.[m]?dst
                stream = '.'.join(fname.split('.')[1:])
                if stream in ('ALL.dst', 'ALLMDST.mdst') :
                    merged[stream] = du
                else :
                    full[stream] = du
        for dudict in [full, merged] + wgs.values() + lines.values() :
            dudict['Total'] = sum(dudict.values())
        wgs['Total'] = full
        # Rename ALL to Calib.
        if 'ALL' in wgs :
            wgs['Calib'] = wgs['ALL']
            del wgs['ALL']
        SumDict.__init__(self, full = full, wgs = wgs, lines = lines, merged = merged)

class StrippingReport(SumDict) :
    '''Full stripping report on retentions and BW.'''

    def __init__(self, dirname = None) :
        if not dirname :
            return
        elif isinstance(dirname, self.__class__) :
            SumDict.__init__(dirname)
            return
        self['dirname'] = [dirname]
        freportname = os.path.join(dirname, 'strippingreport.py')
        with open(freportname) as freport :
            reportdict = eval(freport.read())
        reportdict['stats'] = SumDict(reportdict['stats'])
        for line, stats in reportdict['stats'].iteritems() :
            reportdict['stats'][line] = SumDict(stats)
        SumDict.__init__(self, reportdict)
        self['diskusage'] = DiskUsage(dirname)

    def __add__(self, other) :
        returnval = SumDict.__add__(self, other)
        if not ('stats' in self and 'stats' in other) :
            return returnval
        # Take a weighted average of the timings for each line.
        for name, stat in returnval['stats'].iteritems() :
            try :
                stat['avgtime'] = (self['stats'][name]['avgtime']*self['events'] 
                                   + other['stats'][name]['avgtime']*other['events'])/\
                                   (self['events'] + other['events'])
            except ZeroDivisionError :
                stat['avgtime'] = 0.
        # Recalculate the hlt2 output rate.
        # Could do away with this to avoid redundant info.
        try :
            returnval['hlt2rate'] = returnval['events']/returnval['beamtime']
        except ZeroDivisionError :
            returnval['hlt2rate'] = 0.
        return returnval

    def streams(self) :
        return sorted(self['diskusage']['full'].keys())

    def wgs(self) :
        return sorted(self['diskusage']['wgs'].keys())

    def event_sizes(self) :
        streams = self.streams()
        streams.remove('Total')
        sizes = {}
        for stream in streams :
            decs = self['stats']['StrippingSequenceStream' + stream.split('.')[0]]['decisions']
            if decs > 0 :
                sizes[stream] = self['diskusage']['full'][stream]/decs
            else :
                sizes[stream] = 0.
        return sizes

    def bandwidths(self, unit = 1e6) :
        bws = copy.deepcopy(self['diskusage'])
        # Don't need to include bws['full'] as it's the same dict as bws['wgs']['Total']
        for dudict in [bws['merged']] + bws['wgs'].values() + bws['lines'].values() :
            for key in dudict :
                try :
                    dudict[key] /= self['beamtime'] * unit
                except ZeroDivisionError :
                    dudict[key] = 0.
        return bws

    def wg_bw_table(self) :
        streams = self.streams()
        # make sure total is at the end.
        streams.remove('Total')
        streams.append('Total')
        wgs = self.wgs()
        wgs.remove('Total')
        wgs.append('Total')

        table = r'''\begin{table}
\resizebox{\textwidth}{!}{
\begin{tabular}{''' + ('c' * (len(streams) + 1)) + '}\n'
        table += 'WG / Stream ' 
        for stream in streams :
            table += r' & \rotatebox{90}{' + stream + '} '
        table += r' \\ \hline' + '\n'

        eventsizes = self.event_sizes()
        table += 'Event size [kB]'
        for stream in streams[:-1] :
            table += ' & {0:.1f}'.format(eventsizes[stream]/1000.)

        bws = self.bandwidths()
        table += r' & \\ \hline ' + '\n'
        for wg in wgs :
            table += wg
            for stream in streams :
                try :
                    table += ' & {0:.2f}'.format(bws['wgs'][wg][stream])
                except KeyError :
                    table += ' & -'
            table += r' \\' + '\n'
        table += r'''\end{tabular}
}
\caption{Bandwidth [MB/s], calculated from %s events, equivalent to %.2f s stable beam time given an HLT2 output of %.1f Hz.}
\end{table}''' % (self['events'], self['beamtime'], self['hlt2rate'])
        return table

    def merged_vs_split_bw_table(self) :
        bws = self.bandwidths()
        nominaldstbw = sum(bw for stream, bw in bws['full'].iteritems() if stream.split('.')[-1] == 'dst')
        nominalmdstbw = sum(bw for stream, bw in bws['full'].iteritems() if stream.split('.')[-1] == 'mdst')
        table = r'''\begin{table}
\begin{tabular}{c|ccc}
Output / Streams & Nominal & Single & Per line \\ \hline
DST & ''' + '{0:.2f}'.format(nominaldstbw)
        for bw in bws['merged']['ALL.dst'], bws['lines']['dst']['Total'] :
            table += ' & {0:.2f} ({1:.2f})'.format(bw, bw/nominaldstbw)
        table += r''' \\
mDST & {0:.2f}'''.format(nominalmdstbw)
        for bw in bws['merged']['ALLMDST.mdst'], bws['lines']['mdst']['Total'] :
            table += ' & {0:.2f} ({1:.2f})'.format(bw, bw/nominalmdstbw)
        table += r'''\\
Total & {0:.2f}'''.format(bws['full']['Total'])
        for bw in (bws['merged']['ALL.dst'] + bws['merged']['ALLMDST.mdst']), (bws['lines']['dst']['Total'] + bws['lines']['mdst']['Total']) :
            table += ' & {0:.2f} ({1:.2f})'.format(bw, bw/bws['full']['Total'])
        table += r'''\\
\end{tabular}
\caption{Bandwidth [MB/s] for nominal stream definitions, single DST and mDST streams, and one stream per line. Calculated from %s events, equivalent to %.2f s stable beam time given an HLT2 output of %.1f Hz.}
\end{table}''' % (self['events'], self['beamtime'], self['hlt2rate'])

        return table

    def get_wg_lines(self) :
        reportfile = os.path.join(self['dirname'][0], 'strippingreport.py')
        streamlines = {}
        with open(reportfile) as f :
            stream = None
            for line in f :
                stripname = re.findall("'Stripping.*?'", line)
                if not stripname :
                    continue
                stripname = stripname[0][1:-1]
                if stripname == 'StrippingGlobal' :
                    continue
                if 'StrippingSequenceStream' in stripname :
                    stream = stripname[len('StrippingSequenceStream'):]
                    streamlines[stream] = []
                    continue
                streamlines[stream].append(stripname)
        allwglines = {}
        wgs = self.wgs()
        wgs.remove('Total')
        # Calib <-> ALL
        if 'Calib' in wgs :
            wgs.remove('Calib')
            wgs.append('ALL')
        for wg in wgs :
            allwglines[wg] = {}
            for stream in self.streams() :
                wgstreamname = 'WG_' + wg + '_' + stream.split('.')[0]
                if wgstreamname in streamlines :
                    allwglines[wg][stream] = streamlines[wgstreamname]

        if 'ALL' in wgs :
            allwglines['Calib'] = allwglines['ALL']
            del allwglines['ALL']
        return allwglines

    def make_retentions_table(self, dstlimit = 0.05, mdstlimit = 0.5, timelimit = 1.) :
        allwglines = self.get_wg_lines()
        tables = dict(table = '',
                      hottable = '',
                      zerotable = '',
                      slowtable = '')
        headline = ' |*%-49s*|*N evnts*|*Mult.*|*Rate, %%*|*ms/evt*|\n' % 'Line name'
        statformat = ' |!%-50s| %7d | %5.2f | %7.5f | %6.3f |\n'
        limits = {'dst' : dstlimit, 'mdst' : mdstlimit}
        for wg in sorted(allwglines.keys()) :
            wgheader = '---++ ' + wg + '\n\n'
            wgtabs = dict((tab, '') for tab in tables)
            for stream in sorted(allwglines[wg].keys()) :
                streamheader = '---+++ ' + stream + '\n'
                streamtabs = dict((tab, '') for tab in tables)
                for line in allwglines[wg][stream] :
                    linestats = self['stats'][line]
                    retention = float(linestats['decisions'])/self['events']*100.
                    try :
                        mult = float(linestats['candidates'])/linestats['decisions']
                    except ZeroDivisionError :
                        mult = 0.
                    statsline = statformat % (line, linestats['decisions'], mult, retention, linestats['avgtime'])
                    streamtabs['table'] += statsline
                    if retention > limits[stream.split('.')[-1]] :
                        streamtabs['hottable'] += statsline
                    elif retention == 0. :
                        streamtabs['zerotable'] += statsline
                    if linestats['avgtime'] > timelimit :
                        streamtabs['slowtable'] += statsline
                for tab in tables :
                    if streamtabs[tab] :
                        wgtabs[tab] += streamheader
                        wgtabs[tab] += headline
                        wgtabs[tab] += streamtabs[tab] + '\n'
            for tab in tables :
                if wgtabs[tab] :
                    tables[tab] += wgheader + wgtabs[tab] + '\n'
        
        returnstr = '''N. events: {0}

Stable beam time: {1:.2f} s

HLT2 output rate: {2:.1f} Hz

%TOC%

'''.format(self['events'], self['beamtime'], self['hlt2rate'])

        returnstr += '---+ Global retentions\n\n'
        returnstr += '| Stream | Retention [%] | \n'
        returnstr += '| Global | {0:.2f} |\n'.format(self['stats']['StrippingGlobal']['decisions']*100./self['events'])
        streams = self.streams()
        streams.remove('Total')
        for stream in streams :
            returnstr += '| {0} | {1:.2f} |\n'.format(stream, self['stats']['StrippingSequenceStream' + stream.split('.')[0]]['decisions']*100./self['events'])
        returnstr += '\n'
        returnstr += '---+ Retentions\n\n' + tables['table']
        returnstr += '---+ Lines with high retentions\n\n' + tables['hottable']
        returnstr += '---+ Lines with zero retention\n\n' + tables['zerotable']
        returnstr += '---+ Slow lines\n\n' + tables['slowtable']
        
        return returnstr

def make_report(dirname, *dirnames) :
    return sum((StrippingReport(name) for name in (dirname,) + dirnames),
               StrippingReport())

def find_report_dirs(dirname, *dirnames) :
    if dirnames :
        return reduce(lambda x, y : x + y, 
                      (find_report_dirs(name) for name in (dirname,) + dirnames),
                      [])
    args = ['find', dirname, '-name', 'strippingreport.py']
    proc = subprocess.Popen(args, 
                           stdout = subprocess.PIPE)
    stdout, stderr = proc.communicate()
    if 0 != proc.poll() :
        raise OSError('Failed to call ' + ' '.join(args))
    results = [os.path.dirname(line) for line in stdout.split('\n')]
    proc.stdout.close()
    return results

if __name__ == '__main__' :
    from argparse import ArgumentParser
    import pprint
    argparser = ArgumentParser()
    argparser.add_argument('-d', '--dirs', help = 'Comma separated list of directories to search for stripping reports')
    argparser.add_argument('-p', '--print', action = 'store_true', help = 'Print the report.')
    argparser.add_argument('-b', '--bandwidth', action = 'store_true', help = 'Print the bandwidth table.')
    argparser.add_argument('-m', '--mergedsplitbw', action = 'store_true', help = 'Print table comparing bandwidth between merged and per-line streams.')
    argparser.add_argument('-r', '--retentions', action = 'store_true', help = 'Print the retentions table.')
    argparser.add_argument('--hlt2rate', type = float, default = 0., help = 'Override HLT2 rate in the report.')
    args = argparser.parse_args()

    report = make_report(*find_report_dirs(*args.dirs.split(',')))
    if args.hlt2rate != 0. :
        report['hlt2rate'] = args.hlt2rate
        report['beamtime'] = report['events']/args.hlt2rate
    if getattr(args, 'print') :
        pprint.pprint(report)
    if args.bandwidth :
        print report.wg_bw_table()
    if args.mergedsplitbw :
        print report.merged_vs_split_bw_table()
    if args.retentions :
        print report.make_retentions_table()
