# $Id: $
# Test your line(s) of the stripping by taking the dictionaries from StrippingSettings
#  
# NOTE: Please make a copy of this file for your testing, and do NOT change this one!
#

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

#
#Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
#
from Configurables import RawEventJuggler
juggler = RawEventJuggler( DataOnDemand=True, Input=0.3, Output=4.2 )

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

# Specify the name of your configuration
stripping='stripping27'

## remove GECs for pA
from Configurables import TrackSys
TrackSys().GlobalCuts = { 'Velo':20000, 'IT':999999, 'OT':999999 }

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

# NOTE: this will work only if you inserted correctly the 
# default_config dictionary in the code where your LineBuilder 
# is defined.
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
conf=strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams( stripping = conf, archive = archive)

## add HERSCHEL and VELO raw banks to some lines
for stream in streams:
  for line in stream.lines:
    if line.name() in ['StrippingHeavyIonDiMuonJpsi2MuMuLine', 'StrippingMBMicroBias', 'StrippingMBMicroBiasLowMult', 'StrippingMBNoBias', 'StrippingSingleElectron', 'StrippingHeavyIonOpenCharmD02HHLine', 'StrippingHeavyIonOpenCharmNoPVD02HHBBLine', 'StrippingHeavyIonOpenCharmNoPVD02HHBELine', 'StrippingHeavyIonOpenCharmDst2D0PiLine', 'StrippingHeavyIonOpenCharmDp2KHHLine', 'StrippingHeavyIonOpenCharmDs2KKHLine', 'StrippingHeavyIonOpenCharmLc2PKHLine']:
      if line.RequiredRawEvents: 
        line.RequiredRawEvents += ['HC']
        if 'Velo' not in line.RequiredRawEvents:
          line.RequiredRawEvents += ['Velo']
      else:
        line.RequiredRawEvents = ['HC', 'Velo']

dstStreams  = [ "IFT", "MiniBias" ]
stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = ProcStatusCheck(),
                    TESPrefix = stripTESPrefix,
                    Verbose = True,
                    DSTStreams = dstStreams)

#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements
                                      )


SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }


SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True) }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
#from Configurables import StrippingTCK
#stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x40402500) # boh



#from Configurables import StrippingTCK
#stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112100)

#
#Configure DaVinci
#

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())
sr.ReportFrequency = 1000

from Configurables import AlgorithmCorrelationsAlg
ac = AlgorithmCorrelationsAlg(Algorithms = list(set(sc.selections())))

DaVinci().HistogramFile = 'DV_'+stripping+'_histos.root'
DaVinci().EvtMax = 300000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
#DaVinci().appendToMainSequence( [ ac ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType  = "2016"
DaVinci().InputType = "RDST"


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# database
DaVinci().DDDBtag   = "dddb-20150724"
DaVinci().CondDBtag = "cond-20161011"

# input file
importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco16_pHe.py")
