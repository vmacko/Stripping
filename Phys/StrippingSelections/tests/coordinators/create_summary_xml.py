
from Configurables import DaVinci
from Gaudi.Configuration import importOptions

DaVinci().DataType = '2015'
DaVinci().Lumi = True
DaVinci().EvtMax = 100000

from Configurables import CondDB
CondDB().LatestGlobalTagByDataType = "2015"

from Configurables import LHCbApp
LHCbApp().XMLSummary = "summary.xml"

importOptions("$STRIPPINGSELECTIONSROOT/tests/data/Reco15a_Run164668.py")
