#!/bin/env python

'''Get the breakdown of lines by WG and stream for a given stripping version.'''

import sys, os
sys.path.insert(0, os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 'tests', 'python'))
from StrippingTests.WGLines import get_wg_lines
from StrippingTests.Utils import stripping_args
from pprint import pformat

def main() :
    argparser = stripping_args()
    argparser.add_argument('--outputfile', default = None,
                           help = '''Name of file to which to write the dict of lines.
If None, the dict is output to the console.''')

    args = argparser.parse_args()
    if args.stripping :
        lines = get_wg_lines(args.stripping, args.stripping)
    else :
        lines = get_wg_lines(args.settings, args.archive)
    if args.outputfile :
        with open(args.outputfile, 'w') as f :
            f.write(pformat(lines))
    else :
        print pformat(lines)

if __name__ == '__main__' :
    main()
