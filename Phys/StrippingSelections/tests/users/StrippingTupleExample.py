import sys, os
# This module should really live somewhere more useful, but this'll do for now.
sys.path.insert(0, os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 'tests', 'users'))
from StrippingTuple import add_stripping_tuple_sequence
from Configurables import DaVinci

# Creates the ntuple, adds it to DaVinci, and configures the DataType etc of DaVinci.
decaytuple = add_stripping_tuple_sequence('Stripping29', 'StrippingB2HHBDTLine')

# Then you can add tools to the DecayTreeTuple, and otherwise modify its configuration, as necessary.
# If you want to do something a bit different, you can retrieve the line documentation with
#
# from StrippingDoc import StrippingDoc
# strippingdoc = StrippingDoc('Stripping29')
# linedoc = strippingdoc.get_line('StrippingB2KShh_LL_Run2_OS_Line')
# 
# then use it to manually configure things.
