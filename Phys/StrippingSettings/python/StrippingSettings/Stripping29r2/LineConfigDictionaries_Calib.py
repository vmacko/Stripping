CharmFromBSemiForHadronAsy = {
    "BUILDERTYPE": "CharmFromBSemiForHadronAsyAllLinesConf", 
    "CONFIG": {
        "D0to3H_3pi_DeltaMass_MAX": 350.0, 
        "D0to3H_3pi_MASS_MAX": 1400.0, 
        "D0to3H_3pi_MASS_MIN": 900.0, 
        "D0to3H_B_DIRA_MIN": 0.99, 
        "D0to3H_B_DOCACHI2_MAX": 50.0, 
        "D0to3H_B_MASS_MAX": 4900.0, 
        "D0to3H_B_MASS_MIN": 1800.0, 
        "D0to3H_B_VCHI2NDF_MAX": 15.0, 
        "D0to3H_DOCACHI2_MAX": 10.0, 
        "D0to3H_DZ": 2.0, 
        "D0to3H_K2pi_DeltaMass_MAX": 250.0, 
        "D0to3H_K2pi_MASS_MAX": 1800.0, 
        "D0to3H_K2pi_MASS_MIN": 1300.0, 
        "D0to3H_REQUIRE_TOS": True, 
        "D0to3H_SUMPT_MIN": 1800.0, 
        "D0to3H_VCHI2NDF_MAX": 3.0, 
        "GEC_nLongTrk": 250.0, 
        "GHOSTPROB_MAX": 0.35, 
        "H_PT": 250.0, 
        "K_PIDKMin": 6.0, 
        "Lc2Kpi_B_DIRA_MIN": 0.99, 
        "Lc2Kpi_B_DOCACHI2_MAX": 50.0, 
        "Lc2Kpi_B_FDCHI2_MIN": 20.0, 
        "Lc2Kpi_B_MASS_MAX": 4300.0, 
        "Lc2Kpi_B_MASS_MIN": 2200.0, 
        "Lc2Kpi_B_VCHI2NDF_MAX": 15.0, 
        "Lc2Kpi_DOCACHI2_MAX": 10.0, 
        "Lc2Kpi_DZ": 1.0, 
        "Lc2Kpi_DeltaMass_MAX": 700.0, 
        "Lc2Kpi_FDCHI2_MIN": 20.0, 
        "Lc2Kpi_MASS_MAX": 1350.0, 
        "Lc2Kpi_MASS_MIN": 800.0, 
        "Lc2Kpi_REQUIRE_TOS": True, 
        "Lc2Kpi_SUMPT_MIN": 1500.0, 
        "Lc2Kpi_VCHI2NDF_MAX": 3.0, 
        "MuPiPi_CHI2NDF": 3.0, 
        "MuPiPi_DOCACHI2_MAX": 15.0, 
        "MuPiPi_FDCHI2_MIN": 20.0, 
        "MuPi_CHI2NDOF_MAX": 3.0, 
        "MuPi_DIRA_MIN": -99.0, 
        "MuPi_DOCACHI2_MAX": 8.0, 
        "MuPi_FDCHI2_MIN": 20.0, 
        "MuPi_SUMPT_MIN": 1300.0, 
        "Mu_PT": 800.0, 
        "PiPi_CHI2NDF": 3.0, 
        "PiPi_DOCACHI2_MAX": 15.0, 
        "PiPi_MASS_MAX": 500.0, 
        "PiPi_SUMPT_MIN": 600.0, 
        "Pi_PIDKMax": 6.0, 
        "Slowpi_PIDKMax": 10.0, 
        "Slowpi_PIDeMax": 99.0, 
        "Slowpi_PTMin": 200.0, 
        "prescale_D0to3piRS": 1.0, 
        "prescale_D0to3piWS": 0.2, 
        "prescale_D0toK2piRS": 1.0, 
        "prescale_D0toK2piWS": 0.2, 
        "prescale_LbRS": 1.0, 
        "prescale_LbWS": 0.2
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "ALL" ]
}

D2KPiPi0_PartReco = {
    "BUILDERTYPE": "D2KPiPi0_PartRecoBuilder", 
    "CONFIG": {
        "BDIRA": 0.999, 
        "BFDCHI2HIGH": 100.0, 
        "BPVVDZcut": 0.0, 
        "BVCHI2DOF": 6, 
        "DELTA_MASS_MAX": 190, 
        "ElectronPIDe": 5.0, 
        "ElectronPT": 0, 
        "HadronMINIPCHI2": 9, 
        "HadronPT": 800.0, 
        "KLepMassHigh": 2500, 
        "KLepMassLow": 500, 
        "KaonPIDK": 5.0, 
        "PionPIDK": -1.0, 
        "Slowpion_PIDe": 5, 
        "Slowpion_PT": 300, 
        "Slowpion_TRGHOSTPROB": 0.35, 
        "TOSFilter": {
            "Hlt2CharmHad.*HHX.*Decision%TOS": 0
        }, 
        "TRGHOSTPROB": 0.35
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "ALL" ]
}

DstarD2hhGammaCalib = {
    "BUILDERTYPE": "DstarD2hhGammaCalib", 
    "CONFIG": {
        "CombMassHigh": 2120.0, 
        "CombMassHigh_HH": 1820.0, 
        "CombMassLow": 1380.0, 
        "CombMassLow_HH": 500.0, 
        "Daug_TRCHI2DOF_MAX": 3, 
        "Dstar_AMDiff_MAX": 180.0, 
        "Dstar_MDiff_MAX": 160.0, 
        "Dstar_VCHI2VDOF_MAX": 15.0, 
        "HighPIDK": -1, 
        "Hlt1Filter": None, 
        "Hlt1Tos": {
            "Hlt1.*Track.*Decision%TOS": 0
        }, 
        "Hlt2Filter": None, 
        "Hlt2Tos": {
            "Hlt2CharmHadInclDst2PiD02HHXBDTDecision%TOS": 0, 
            "Hlt2PhiIncPhiDecision%TOS": 0
        }, 
        "LowPIDK": 5, 
        "MassHigh": 2100.0, 
        "MassHigh_HH": 1810.0, 
        "MassLow": 1400.0, 
        "MassLow_HH": 600.0, 
        "MaxADOCACHI2": 10.0, 
        "MaxIPChi2": 15, 
        "MaxVCHI2NDOF": 10.0, 
        "MaxVCHI2NDOF_HH": 10.0, 
        "MinBPVDIRA": 0.995, 
        "MinBPVTAU": 0.0001, 
        "MinCombPT": 2000.0, 
        "MinPT": 2500.0, 
        "MinTrkIPChi2": 10, 
        "MinTrkPT": 500.0, 
        "MinVDCHI2_HH": 1000.0, 
        "MinVDCHI2_HHComb": 1000.0, 
        "PiSoft_PT_MIN": 250.0, 
        "PrescaleDstarD2KKGamma": 1, 
        "PrescaleDstarD2KPiGamma": 1, 
        "TrChi2": 4, 
        "TrGhostProb": 0.5, 
        "photonPT": 200.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "Charm" ]
}

ElectronRecoEff = {
    "BUILDERTYPE": "StrippingElectronRecoEffLines", 
    "CONFIG": {
        "DetachedEEK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 22, 
            "bmass_ip_constraint": -3.0, 
            "overlapCut": 0.95, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DetachedEK": {
            "AM": 5000.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1TrackMVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 36, 
            "bCandFlightDist": 4.0
        }, 
        "DetachedMuK": {
            "AM": 5000.0, 
            "DIRA": 0.95, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2": 10, 
            "VDCHI2": 100, 
            "bCandFlightDist": 4.0
        }, 
        "DetachedMuMuK": {
            "AMTAP": 6000.0, 
            "MHIGH": 5700.0, 
            "MLOW": 5000.0, 
            "TisTosSpec": {
                "Hlt1Track(Muon)?MVA.*Decision%TOS": 0
            }, 
            "VCHI2TAP": 20, 
            "bmass_ip_constraint": -3.0, 
            "probePcutMax": 150000.0, 
            "probePcutMin": 750.0
        }, 
        "DoVeloDecoding": False, 
        "EtaMaxVelo": 5.1, 
        "EtaMinVelo": 1.9, 
        "Hlt1Req": {
            "DetachedEEK": "HLT_PASS_RE('Hlt1TrackMVA.*Decision')", 
            "DetachedEK": "HLT_PASS_RE('Hlt1TrackMVA.*Decision')", 
            "DetachedMuK": "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')", 
            "DetachedMuMuK": "HLT_PASS_RE('Hlt1Track(Muon)?MVA.*Decision')"
        }, 
        "Hlt2Req": {
            "DetachedEK": "HLT_PASS_RE('Hlt2.*Topo.*Decision')", 
            "DetachedMuK": "HLT_PASS_RE('Hlt2.*Topo.*Decision')"
        }, 
        "L0Req": {
            "DetachedEEK": "L0_CHANNEL('Electron')", 
            "DetachedEK": "L0_CHANNEL_RE('Electron|Hadron')", 
            "DetachedEPi": "L0_CHANNEL('Electron')", 
            "DetachedMuK": "L0_CHANNEL_RE('Muon|Hadron')", 
            "DetachedMuMuK": "L0_CHANNEL('Muon')", 
            "DetachedMuPi": "L0_CHANNEL('Muon')"
        }, 
        "SharedChild": {
            "EtaMaxEle": 5.1, 
            "EtaMaxMu": 5.1, 
            "EtaMinEle": 1.8, 
            "EtaMinMu": 1.8, 
            "IPChi2Ele": 12, 
            "IPChi2Kaon": 12, 
            "IPChi2Mu": 12, 
            "IPChi2Pion": 36, 
            "IPEle": 0.0, 
            "IPKaon": 0.0, 
            "IPMu": 0.0, 
            "IPPion": 0.0, 
            "ProbNNe": 0.2, 
            "ProbNNk": 0.2, 
            "ProbNNmu": 0.5, 
            "ProbNNpi": 0.8, 
            "PtEle": 1200.0, 
            "PtKaon": 500.0, 
            "PtMu": 1200.0, 
            "PtPion": 1000.0, 
            "TrChi2Ele": 5, 
            "TrChi2Kaon": 5, 
            "TrChi2Mu": 5, 
            "TrChi2Pion": 5
        }, 
        "TrackGEC": 120, 
        "VeloFitter": "SimplifiedGeometry", 
        "VeloMINIP": 0.035, 
        "VeloTrackChi2": 5.0
    }, 
    "STREAMS": [ "BhadronCompleteEvent" ], 
    "WGs": [ "ALL" ]
}

TrackEffD0ToK3Pi = {
    "BUILDERTYPE": "TrackEffD0ToK3PiAllLinesConf", 
    "CONFIG": {
        "D0_MAX_DOCA": 0.05, 
        "D0_MIN_FD": 5.0, 
        "DoVeloDecoding": False, 
        "Dst_MAX_DTFCHI2": 3.0, 
        "Dst_MAX_M": 2035.0000000000002, 
        "HLT2": "HLT_PASS_RE('Hlt2.*CharmHad.*HHX.*Decision')", 
        "Kaon_MIN_PIDK": 7, 
        "LC_MIN_FD": 2.0, 
        "Pion_MAX_PIDK": 4, 
        "RequireDstFirst": False, 
        "Sc_MAX_DTFCHI2": 3.0, 
        "Sc_MAX_M": 2500.0, 
        "TTSpecs": {
            "Hlt2.*CharmHad.*HHX.*Decision%TOS": 0
        }, 
        "VeloFitter": "SimplifiedGeometry", 
        "VeloLineForTiming": False, 
        "VeloMINIP": 0.05
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "ALL" ]
}

TrackEffD0ToKPi = {
    "BUILDERTYPE": "TrackEffD0ToKPiAllLinesConf", 
    "CONFIG": {
        "Dst_DTFCHI2_MAX": 10, 
        "Dst_M_MAX": 2100, 
        "HLT1": "HLT_PASS_RE('Hlt1TrackMVADecision')", 
        "HLT2": "HLT_PASS_RE('Hlt2TrackEff_D0.*Decision')", 
        "Kaon_MIN_PIDK": 0, 
        "Monitor": False, 
        "Pion_MAX_PIDK": 20, 
        "TTSpecs": {
            "Hlt1TrackMVADecision%TOS": 0, 
            "Hlt2TrackEff_D0.*Decision%TOS": 0
        }, 
        "Tag_MIN_PT": 1000.0, 
        "VeloMINIPCHI2": 4.0
    }, 
    "STREAMS": [ "CharmCompleteEvent" ], 
    "WGs": [ "ALL" ]
}

TrackEffMuonTT = {
    "BUILDERTYPE": "StrippingTrackEffMuonTTConf", 
    "CONFIG": {
        "BJpsiKHlt2TriggersTOS": {
            "Hlt2TopoMu2BodyBBDTDecision%TOS": 0
        }, 
        "BJpsiKHlt2TriggersTUS": {
            "Hlt2TopoMu2BodyBBDTDecision%TUS": 0
        }, 
        "BJpsiKMINIP": 10000, 
        "BJpsiKPrescale": 1, 
        "BMassWin": 500, 
        "Hlt1PassOnAll": True, 
        "JpsiHlt1Filter": "Hlt1.*Decision", 
        "JpsiHlt1Triggers": {
            "Hlt1TrackMuonDecision%TOS": 0
        }, 
        "JpsiHlt2Filter": "Hlt2.*Decision", 
        "JpsiHlt2Triggers": {
            "Hlt2SingleMuon.*Decision%TOS": 0, 
            "Hlt2TrackEffDiMuonMuonTT.*Decision%TOS": 0
        }, 
        "JpsiLongMuonMinIP": 0.5, 
        "JpsiLongMuonTrackCHI2": 5, 
        "JpsiLongPT": 1300, 
        "JpsiMINIP": 3, 
        "JpsiMassWin": 500, 
        "JpsiMuonTTPT": 0, 
        "JpsiPT": 1000, 
        "JpsiPrescale": 1, 
        "LongMuonPID": 2, 
        "Postscale": 1, 
        "UpsilonHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "UpsilonHlt2Triggers": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        }, 
        "UpsilonLongMuonMinIP": 0, 
        "UpsilonLongMuonTrackCHI2": 5, 
        "UpsilonLongPT": 1000, 
        "UpsilonMINIP": 10000, 
        "UpsilonMassWin": 1500, 
        "UpsilonMuonTTPT": 500, 
        "UpsilonPT": 0, 
        "UpsilonPrescale": 1, 
        "VertexChi2": 5, 
        "ZHlt1Triggers": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        }, 
        "ZHlt2Triggers": {
            "Hlt2EWSingleMuonVHighPtDecision%TOS": 0
        }, 
        "ZLongMuonMinIP": 0, 
        "ZLongMuonTrackCHI2": 5, 
        "ZLongPT": 10000, 
        "ZMINIP": 10000, 
        "ZMassWin": 40000, 
        "ZMuonTTPT": 500, 
        "ZPT": 0, 
        "ZPrescale": 1
    }, 
    "STREAMS": {
        "Dimuon": [
            "StrippingTrackEffMuonTT_JpsiLine1", 
            "StrippingTrackEffMuonTT_JpsiLine2", 
            "StrippingTrackEffMuonTT_UpsilonLine1", 
            "StrippingTrackEffMuonTT_UpsilonLine2", 
            "StrippingTrackEffMuonTT_ZLine1", 
            "StrippingTrackEffMuonTT_ZLine2", 
            "StrippingTrackEffMuonTT_BJpsiKLine1", 
            "StrippingTrackEffMuonTT_BJpsiKLine2"
        ]
    }, 
    "WGs": [ "ALL" ]
}

TrackEffVeloMuon = {
    "BUILDERTYPE": "StrippingTrackEffVeloMuonConf",
    "CONFIG": {
        "HLT1PassOnAll": True,
        "HLT1TisTosSpecs": {
            "Hlt1SingleMuonNoIPDecision%TOS": 0,
            "Hlt1TrackMuonDecision%TOS": 0
        },
        "HLT2PassOnAll": False,
        "HLT2TisTosSpecs": {
            "Hlt2SingleMuon.*Decision%TOS": 0,
        },
        "JpsiHlt1Filter": "Hlt1.*Decision",
        "JpsiHlt2Filter": "Hlt2.*Decision",
        "JpsiPt": 0.5,
        "LongP": 7.0,
        "MassPostComb": 400.0,
        "MassPreComb": 1000.0,
        "MuDLL": 1.0,
        "Postscale": 1.0,
        "Prescale": 1.0,
        "TrChi2LongMu": 3.0,
        "TrChi2VeMu": 5.0,
        "TrP": 5.0,
        "TrPt": 100.0,
        "UpsilonHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        },
        "UpsilonHLT2TisTosSpecs": {
            "Hlt2SingleMuonLowPTDecision%TOS": 0
        },
       "UpsilonMassPostComb": 1500.0,
        "UpsilonMassPreComb": 100000.0,
        "UpsilonPostscale": 1.0,
        "UpsilonPrescale": 1.0,
        "UpsilonPt": 0.5,
        "UpsilonTrP": 0.0,
        "UpsilonTrPt": 500.0,
        "UpsilonVertChi2": 10000.0,
        "VertChi2": 2.0,
        "ZHLT1TisTosSpecs": {
            "Hlt1SingleMuonHighPTDecision%TOS": 0
        },
        "ZHLT2TisTosSpecs": {
            "Hlt2SingleMuonHighPTDecision%TOS": 0
        },
        "ZMassPostComb": 40000.0,
        "ZMassPreComb": 100000.0,
        "ZPostscale": 1.0,
        "ZPrescale": 1.0,
        "ZPt": 0.5,
        "ZTrMaxEta": 4.5,
        "ZTrMinEta": 2.0,
        "ZTrP": 0.0,
        "ZTrPt": 20000.0,
        "ZVertChi2": 10000.0
    },
    "STREAMS": [ "Dimuon" ],
    "WGs": [ "ALL" ]
}
